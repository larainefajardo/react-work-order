const myConfig = { 
    // SERVER_URL : "https://generic_api.dice205.asia",
    SERVER_URL : "http://localhost:2000",
    SITE_URL : "https://genericpwa.dice205.asia",
    PRIVACY_URL : "https://salto.com.ph/pilmico-data-privacy-policy/",
    CACHE_STATIC : "version_static_aug20",
    CACHE_DYNAMIC : "version_dynamic_aug20",   
    //Generate this using "npm run web-push generate-vapid-keys" in the backend node,
    WEBSITE_NAME: 'Generic Website',
    SAFARI_WEB_PUSH_ID: 'web.dice205.generic.push',
    WEB_PUSH_PUBLIC_KEY: 'BMsvaIgx1T_eiU-3JO4r34JW__FEatmVpXEj3N84lWqMvG9T4hQWdMAgIuhvMIJ9SU4LwsBl11iQ-bDS6wHBk34',
    //Types of push notifications
    DEFAULT_PUSH_TYPE: 'default_push_type',
    SAFARI_PUSH_TYPE : 'safari_push_type',
    sidebarContext : undefined,
    FACEBOOK_APP_ID : '241181960086758',
    FACEBOOK_APP_VERSION : 'v3.1',
    role: [
            {
                role_Id : 1,
                permissions: [ "general_read", "general_create","general_update","general_delete","general_report",
                               "admin_read","admin_create","admin_update","admin_delete","admin_report"],
                name : 'Super Admin',
            },
            {
                role_Id : 2,
                permissions: [ "general_read", "general_create","general_update","general_delete","general_report"],
                name : 'Subscriber',
            },
            {
                role_Id : 3,
                permissions: [ "general_read", "general_report"],
                name : 'Report',
            }
         ],
}