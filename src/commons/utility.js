
import { openDb, deleteDb } from 'idb';
var dbPromise = openDb('generic', 1, db => {
  if (!db.objectStoreNames.contains('users')) {
    db.createObjectStore('users', {keyPath: 'id'});
  }
  if (!db.objectStoreNames.contains('sync-posts')) {
    db.createObjectStore('sync-posts', {keyPath: 'id'});
  }
});

let jsonCheck = (testString) => {
  try
  {
    var json = JSON.parse(testString);
    return true;
  }
  catch(e)
  {
    return false;
  }
}

let convertToHttps = (url) => {
  if(url == undefined || url == null) {
    return url;
  }else {
    return url.replace("http://", "https://");

  }
}

let writeData = (st, data) => {
  return dbPromise
    .then(function(db) {
      var tx = db.transaction(st, 'readwrite');
      var store = tx.objectStore(st);
      store.put(data);
      return tx.complete;
    });
}

let readAllData = (st) => {
  return dbPromise
    .then(function(db) {
      var tx = db.transaction(st, 'readonly');
      var store = tx.objectStore(st);
      return store.getAll();
    });
}

let clearAllData = (st) => {
  return dbPromise
    .then(function(db) {
      var tx = db.transaction(st, 'readwrite');
      var store = tx.objectStore(st);
      store.clear();
      return tx.complete;
    });
}

let addDate = (targetDate, numberOfDays) => {
  let newdate = new Date(targetDate);
  newdate.setDate(newdate.getDate() + numberOfDays);
  return newdate;
}

let subtractDate = (targetDate, numberOfDays) => {
  let newdate = new Date(targetDate);
  newdate.setDate(newdate.getDate() - numberOfDays);
  return newdate;
}

let normalDate = (my_date) => {
  let normal_date = new Date(my_date);
  // console.log(normal_date.getMonth())
  // let month = parseInt(normal_date.getMonth());
  // return  (month + 1 ) + '/' + normal_date.getDate() + '/' + normal_date.getFullYear();
  return normal_date;
}

let formatDateByNumber = (my_date) => {
  let date = new Date(my_date);
  var yr = date.getFullYear();
  var dd =  date.getDate();
  var mm = date.getMonth() + 1; 

  return yr + "-" + mm + "-" + dd ;
}


let prettifyDate = (my_date) => {
  let date = new Date(my_date);
  let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  return  months[date.getMonth()]  + ' ' + date.getDate() + ', ' + date.getFullYear();
}

let passwordFormat = (pass) => {
  var code, i, len;
  var foundNumbers = false;
  var foundCharacters = false;
  var foundSpecialChar = false;
  var foundMinimumLength = false;
  if(pass.length >= 8){
    foundMinimumLength = true;
  }
  for (i = 0, len = pass.length; i < len; i++) {
    code = pass.charCodeAt(i);
    if ((code > 64 && code < 91) || // upper alpha (A-Z)
        (code > 96 && code < 123)) { // lower alpha (a-z)
      foundCharacters = true;
    }else if (code > 47 && code < 58) { // numeric (0-9) 
      foundNumbers = true;
    }else if((code > 32 && code < 48) || 
              (code > 59 && code < 65) ||
              (code > 90 && code < 97) ||
              (code > 122 && code < 127) ) {
      foundSpecialChar = true;
    }
  }
  return foundCharacters && foundNumbers && foundSpecialChar && foundMinimumLength;
}

let jsonToURI = (data) => {
  let query = "";
  for (var key in data) {
    query += encodeURIComponent(key)+"="+encodeURIComponent(data[key])+"&";
  }
  return query;
}

let urlBase64ToUint8Array = (base64String) => {
  var padding = '='.repeat((4 - base64String.length % 4) % 4);
  var base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  var rawData = window.atob(base64);
  var outputArray = new Uint8Array(rawData.length);

  for (var i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

let emailFormat = (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  
  return re.test(email.toLowerCase());
}

export {emailFormat, urlBase64ToUint8Array,jsonToURI,  passwordFormat, formatDateByNumber, writeData, readAllData, clearAllData, addDate, subtractDate, prettifyDate,normalDate, convertToHttps, jsonCheck};
