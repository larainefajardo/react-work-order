export const menuConfig = [
    {
        pageId: 1,
        pageLabel : 'Home',
        isActive  : 'active',
        pageModule : 'home',
        icon : 'sort',
        permission: ["general_read", "general_create", "general_update", "general_delete", "general_report", "admin_read", "admin_create", "admin_update", "admin_delete", "admin_report"]
    },
    {
        pageId: 2,
        pageLabel : 'Dashboard',
        isActive  : '',
        pageModule : 'search',
        icon : 'dashboard',
        permission: ["general_read", "general_create", "general_update", "general_delete", "general_report", "admin_read", "admin_create", "admin_update", "admin_delete", "admin_report"]
    },
    {
        pageId : 3,
        pageLabel : 'Calendar',
        isActive  : '',
        pageModule : 'manageUser',
        icon : 'event_note',
        permission: ["admin_read", "admin_create", "admin_update", "admin_delete"]
    },
    {
        pageId : 4,
        pageLabel : 'Upload Files',
        isActive  : '',
        pageModule : 'notifications',
        icon : 'cloud_upload',
        permission: ["admin_read", "admin_create", "admin_update", "admin_delete"]
    },
    {
        pageId : 5,
        pageLabel : "Project List",
        isActive  : '',
        pageModule : 'inbox',
        icon : 'list',
        permission: ["admin_read", "admin_create", "admin_update", "admin_delete"]
    },
];
