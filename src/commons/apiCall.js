import { writeData, readAllData, formatDateByNumber, jsonToURI } from './utility.js';
import { myConfig } from './config.js';
import { endpoints } from './restEndpoints.js';
const idb = require('idb');

let apiModule = async (my_module, requestParams ,formData) => {
  let url = endpoints(requestParams)[my_module].url;
  let headers = endpoints(requestParams)[my_module].headers;
  let method = endpoints(requestParams)[my_module].method;
  let server_url = myConfig.SERVER_URL;
  if(endpoints(requestParams)[my_module].server_url !== null && 
    endpoints(requestParams)[my_module].server_url !== undefined)
    {
      server_url = endpoints(requestParams)[my_module].server_url; 
    }
  let result = {};
  if ('serviceWorker' in navigator && 'SyncManager' in window && !navigator.onLine && endpoints(requestParams)[my_module].sync == true) {
    return offlineSync('authentication_register',{}, formData);
  } else {  
     let data = jsonToURI(formData);
    return new Promise(function(resolve, reject) {
      if(data === null || data === '') {
        fetch(server_url + url, {
          method: method,
          headers: headers
        })
        .then(function(response){
        console.log(response);
        return response.json();
        })
        .then(function(data) {
          console.log(data)
            resolve(data);
        })
        .catch(function(err) {
          console.log(err);
          window.confirm(err);
          reject(err);
        });
      }else {
        fetch(server_url + url, {
          method: method,
          headers: headers,
          body:  data
        })
        .then(function(response){
        console.log(response);
        return response.json();
        })
        .then(function(data) {
          console.log(data)
            resolve(data);
        })
        .catch(function(err) {
          console.log(err);
          window.confirm(err);
          reject(err);
        });
   
      }
    });  
  }
}

let apiCall = (url, method, headers, formData) => {
    return new Promise(function(resolve, reject) {
      fetch(myConfig.SERVER_URL + url, {
          method: method,
          headers: headers,
          body:  formData
        })
        .then(function(response){
        console.log(response);
        return response.json();
        })
        .then(function(data) {
          console.log(data)
            resolve(data);
        })
        .catch(function(err) {
          console.log(err);
          window.confirm(err);
          reject(err);
        });
    });
}

let offlineSync = (my_module, requestParams ,formData) => {
  let url = endpoints(requestParams)[my_module].url;
  let headers = endpoints(requestParams)[my_module].headers;
  let method = endpoints(requestParams)[my_module].method;
  let post = {};
  post.requestParams = {url : url, method : method, headers : headers};
  post.data = { formData };
  let id = 0;
  return new Promise(function(resolve, reject) {
    navigator.serviceWorker.ready
      .then(function(sw) {
        readAllData('sync-posts')
          .then(function(sync){
            id = sync.length + 1;
            return id;
          })
          .then(function(id){
            writeData('sync-posts', {post , id : id})
              .then(function() {
                return sw.sync.register('sync-new-posts');
              })
              
          resolve({message : 'Post is saved for syncing', result : 'success'});
          return({message : 'Post is saved for syncing', result : 'success'});
              
          });
    });
  });
}

export { apiCall, offlineSync, apiModule };