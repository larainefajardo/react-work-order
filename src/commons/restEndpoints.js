export let endpoints = (requestParams = {}) => {

   return { 
        authentication_login: {
            description: 'Login user to the system',
            url: '/api/authentication/login_no_client',
            method: 'POST',
            headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
            sync : false
        },
        authentication_register: {
            description: 'Registers a new user to the system',
            url : '/api/authentication/register',
            method: 'POST',
            headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
            sync : true
        },
        authentication_forgot_password:{
            description: 'Retrieve password through email',
            url : '/api/authentication/forgotPassword',
            method : 'POST',
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' },
            sync : false
        },
        authentication_reset_password:{
            description: 'Reset user password',
            url : '/api/authentication/resetpass',
            method : 'POST',
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' },
            sync : false
        },
        user_store_vapid: {
            description: 'Stores the VAPID of the user for push notifications',
            url : '/api/notification/store_vapid',
            method: 'POST',
            headers: {'Content-Type' : 'application/x-www-form-urlencoded', 'x-auth':`${requestParams.x_auth}`},
            sync : false
        },
        user_list_all: {
            description: 'List all the users in the system',
            url : '/api/user/all',
            method: 'GET',
            headers: {'x-auth':`${requestParams.x_auth}`},
            sync : false
        },
        /**** NOTIFICATION RELATED  *****/
        send_notification : {
            description: 'Send notifications to the users within the given ID',
            url : '/api/notification/send_notification',
            method: 'POST',
            headers: {'Content-Type' : 'application/x-www-form-urlencoded', 'x-auth':`${requestParams.x_auth}`},
            sync : true

        },
        profile_inbox : {
            description: 'Retrieves the logged in users inbox',
            url : '/api/profile/inbox',
            method: 'GET',
            headers: {'x-auth':`${requestParams.x_auth}`},
            sync : false
        },
        /**** MANAGE USERS MODULE  *****/
        admin_get_all_users : {
            description: 'Get all users list for manage user module',
            url: '/api/user/all',
            method: 'GET',
            headers: { 'x-auth' : `${requestParams.x_auth}` },
            sync : false
        },
        admin_update_user : {
            description: 'Update user in manage user module',
            url: `/api/user/${requestParams.user_id}`,
            method: 'PUT',
            headers: { 'Content-Type' : 'application/x-www-form-urlencoded', 'x-auth' : `${requestParams.x_auth}` },
            sync : true
        },
        admin_create_user : {
            description: 'Create user in manage user module',
            url: `/api/user/`,
            method: 'POST',
            headers: { 'Content-Type' : 'application/x-www-form-urlencoded', 'x-auth' : `${requestParams.x_auth}` },
            sync : true
        },
        admin_search_user : {
            description: 'Search user in manage user module',
            url: '/api/user/search',
            method: 'POST',
            headers: { 'Content-Type' : 'application/x-www-form-urlencoded', 'x-auth' : `${requestParams.x_auth}` },
            sync : false
        },
        admin_delete_user : {
            description: 'Delete user in manage user module',
            url: `/api/user/${requestParams.user_id}`,
            method: 'DELETE',
            headers: { 'x-auth' : `${requestParams.x_auth}` },
            sync : false
        },
        /**** NOTIFICATION INBOX ****/
        user_notification_inbox : {
            description: 'User notification inbox',
            url: `/api/notification/inbox/${requestParams.user_id}`,
            method: 'GET',
            headers: { 'x-auth' : `${requestParams.x_auth}` },
            sync : false
        },
        /*******USER PROFILE**********/
        user_profile_picture: {
            description : 'Upload user profile picture',
            url : '/api/upload/',
            method: 'POST',
            headers :  { 'x-auth' : `${requestParams.x_auth}`, 'content-type': 'multipart/form-data' },
            sync : false
        }
    }

}