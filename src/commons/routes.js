import React from 'react';
import Login from '../components/login.js';
import SidebarContainer from '../components/sidebarContainer.js';
import Home from '../pages/home.js';
import Search from '../pages/search.js';
import Profile from '../pages/profile.js';
import Aux from '../components/aux.js';
import SignUp from '../components/signUp.js';
import ManageUser from '../pages/manageUser.js';
import Notifications from '../pages/notifications.js';
import ForgotPassword from '../components/forgotPassword.js';
import ResetPassword from '../components/resetPassword.js';
import { writeData, readAllData, clearAllData } from './utility.js';
import { BrowserRouter as Router, Link, Route, Redirect } from 'react-router-dom'; 
import { myConfig } from './config.js';
import { menuConfig } from './menu.js';

import queryString  from 'query-string';
var Homes = require('../pages/home');
let componentsToUse = [];
const Routes = (props) => {
  let fakeAuth = {
    isAuthenticated: false,
    authenticate(cb) {
      this.isAuthenticated = true;
      setTimeout(cb, 100); // fake async
    },
    signout(cb) {
      this.isAuthenticated = false;
      setTimeout(cb, 100);
    }
  };
    
  let storeSession = {
    isAuthenticated: {},
    authenticate(session) {
      this.isAuthenticated = session;
    }
  };

  let getActiveMenu = (label) => {
    let result;
    menuConfig.map((item, i) => {
        if(label.toUpperCase() == item.pageLabel.toUpperCase()){
        menuConfig[i].isActive = 'active';
        result = item.pageModule;
        }else{
        menuConfig[i].isActive = '';
        }
    });
    return result;
    }

  let menuRoute = (that) => {
    let url = that.props.match.params.name.toUpperCase().replace('-', ' ');
    let pageModule = getActiveMenu(url);
    
    if(componentsToUse[pageModule] == null) {
      console.log("pageModule :" + pageModule);
      const ComponentToRender = require("../pages/" + pageModule).default;
      componentsToUse[pageModule] = (<ComponentToRender 
                  parent={that}
                  toggleSidebar={() => props.toggleSidebar()}
                  menuRoute={(index) => menuRoute(index)}
                  auth={fakeAuth}
                  clientId = {props.clientId}
                  setUserSession={(user) => props.setUserSession(user)}
                  changePageHandler={(pageId) => props.changePageHandler(pageId)}
                  getSession={() => props.getSession()}
                  location={props.location}/>);
    }
        return (componentsToUse[pageModule]);
  }

  let RenderPage = () => {
    return (
      <div>
      {/**********************SIGN UP**********************/}
      <Route exact path="/register" render={() => 
        <SignUp
        changePageHandler={(pageId) => props.changePageHandler(pageId)}
        setUserSession={(user) => props.setUserSession(user)}
        getSession={() => props.getSession()} /> 
      }/>
      {/**********************LOGIN**********************/}
        <Route exact path="/"  render={() => 
          // ( props.getSession().user == null || props.getSession().user == undefined && props.location != undefined ?
          ( !fakeAuth.isAuthenticated && (props.getSession().user == null || props.getSession().user == undefined) ?
              ( <Login
                changePageHandler={(pageId) => props.changePageHandler(pageId)}
                setUserSession={(user) => props.setUserSession(user)}
                getSession={() => props.getSession()}
                location={props.location}
                session={storeSession}
                auth={fakeAuth}/> 
                ) :
              (<Redirect to="/dashboard/home" />) 
          ) } />
      {/**********************HOME**********************/}
        <Route path="/dashboard/:name" render={() => 
          ( !fakeAuth.isAuthenticated  && (props.getSession().user == null || props.getSession().user == undefined )  ?
              (<Redirect to="/" />) : 
              (<SidebarContainer
                toggleSidebar={() => props.toggleSidebar()}
                sidebar={props.toggleDrawer}
                menuRoute={( that) => menuRoute(that)}
                auth={fakeAuth}
                clientId = {props.clientId}
                setUserSession={(user) => props.setUserSession(user)}
                changePageHandler={(pageId) => props.changePageHandler(pageId)}
                getSession={() => props.getSession()}
                location={props.location}
              />)
          ) } />
      {/**********************PROFILE**********************/}
        <Route exact path="/profile" render={() => 
          ( !fakeAuth.isAuthenticated  && (props.getSession().user == null || props.getSession().user == undefined )  ?
              (<Redirect to="/" />) : 
              (<Profile
                toggleSidebar={() => props.toggleSidebar()}
                sidebar={props.toggleDrawer}
                menuRoute={(that) => menuRoute(that)}
                auth={fakeAuth}
                clientId = {props.clientId}
                setUserSession={(user) => props.setUserSession(user)}
                changePageHandler={(pageId) => props.changePageHandler(pageId)}
                getSession={() => props.getSession()}
                location={props.location}
              />)
          ) } />
      {/**********************FORGOT PASSWORD**********************/}
        <Route path="/forgot-password" render={() => 
            (<ForgotPassword
            changePageHandler={(pageId) => props.changePageHandler(pageId)}
            setUserSession={(user) => props.setUserSession(user)}
            getSession={() => props.getSession()}/>)
          } />
      </div>
      )
  }

	let content = null;
  const parsed = queryString.parseUrl(window.location.href);
  console.log(parsed.query.token);
  //If there are no token query string. Use paths in RenderPage
  if(parsed.query == null || parsed.query.token == undefined) {
    content = (
      <RenderPage/>
    );
  //Else prompt the resetpassword screen  
  }else{
    content = (
      <ResetPassword
        token={parsed.query.token}
        changePageHandler={(pageId) => props.changePageHandler(pageId)}
        setUserSession={(user) => props.setUserSession(user)}
        getSession={() => props.getSession()}/>
        )
  }

  return (
        <Aux>
          <Router>
            <div>
             {content}
             
            </div>  
          </Router>
        </Aux>
  )

}

export default Routes;