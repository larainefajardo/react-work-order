import React, { Component } from 'react';
import '../App.css';
import { writeData, passwordFormat, jsonToURI } from '../commons/utility.js';
import { apiModule, offlineSync } from '../commons/apiCall.js';
import Aux from './aux.js';
import { styles } from '../styles/styles.js';
import { BrowserRouter as Router, Route, Link, withRouter, Redirect } from 'react-router-dom';
import { myConfig } from '../commons/config.js';
class SignUp extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      isLoading: false, 
      classes : [], 
      userClass : "",
      validate: [],
      gender : '',
      passClasses : "input100",
      disabled : false,
      iconMove : "drop-icon-hide",
      openList : false,
      privacy : false,
      feeds: true,
      redirect: false,
      errorMsg : 'Please fill required fields',
      errorFields: {username: true, password : true, confirm_password : true,confirm_email : true,
                    email: true, first_name : true, last_name : true, mobile_number: true,
                    gender: true, country : true, city : true },
      fields: {password : '', confirm_password : '',
              confirm_email:'', email: '', first_name : '', last_name : '', mobile_number: '',
              male : false, female: false, country : '', city : '', gender : '',
              },
      dropClass : "wrap-input100 validate-input m-b-50 drop-icon-hide" , 
    };
    this.handleChanges = this.handleChanges.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClasses = this.handleClasses.bind(this);
    this.handleValidation = this.handleValidation.bind(this);
  }

  handleChanges (field, e) {
    let fields = this.state.fields;
        fields[e.target.name] = e.target.value;        
        this.setState({fields});
  }

  handleClasses (event, num) {
    const classItems = this.state.classes;
    const validateItems = this.state.validate;
    if(event != "" && event != undefined){
      classItems[num] = "has-val";
       validateItems[num] = "";
    }else{
       classItems[num] = "";
       validateItems[num] = "alert-validate";
    }
    // update state
    this.setState({
        classItems,
        validateItems
    });
  }

  handleValidation (value, num) {
    const validateItems = this.state.validate;
    validateItems[num] = " alert-validate ";
    
    // update state
    this.setState({
        validateItems
    });
  }


  validateFields (fields)  {
    let error = this.state.errorFields;
    
    let msg = '';

    if(fields["male"] == false && fields["female"] == false){
      error["gender"] = false;
      msg = "Gender is required";
    }  

    let passRegEx = /^(?=.*[_$@.])(?=.*[^_$@.])[\w$@.]{8,15}$/;
    if(fields["password"] == ""){
      this.handleValidation(fields["password"], 1);
      error["password"] = false;
    }
    if(!passwordFormat(fields["password"])){
      this.handleValidation(fields["password"], 1);
      error["password"] = false;
      msg = 'Password invalid';
    }
    if(fields["confirm_password"] == ""){
      this.handleValidation(fields["confirm_password"], 2);
      error["confirm_password"] = false;
    }
    if(!passwordFormat(fields["confirm_password"])){
      this.handleValidation(fields["confirm_password"], 2);
      error["confirm_password"] = false;
      msg = 'Password invalid';
    }
    if (fields["confirm_email"] == ""){
      this.handleValidation(fields["confirm_email"], 10);
      error["confirm_email"] = false;
      msg = "Email invalid";
    }
    if (fields["confirm_email"] !== fields["email"]){
      this.handleValidation(fields["confirm_email"], 10);
      error["confirm_email"] = false;
      msg = "Email invalid";
    }
    
    if (fields["email"] == "") {
      this.handleValidation(fields["email"], 3);
      error["email"] = false;
      msg = "Email invalid";
    }
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(fields["email"].toLowerCase())) {
     this.handleValidation(fields["email"], 3);
     error["email"] = false;
     msg = "Email invalid";
    }

    if (fields["first_name"] == ""){
      this.handleClasses(fields["first_name"], 4);
      error["first_name"] = false;
      msg = "Please fill required fields";
    }

    if (fields["last_name"] == ""){
      this.handleClasses(fields["last_name"], 5);
      error["last_name"] = false;
      msg = "Please fill required fields";
    }

    if (fields["mobile_number"] == ""){
      this.handleClasses(fields["mobile_number"], 6);
      error["mobile_number"] = false;
      msg = "Please fill required fields";
    }

    if (fields["country"] == ""){
      this.handleClasses(fields["country"], 7);
      error["country"] = false;
      msg = "Please fill required fields";
    }

    if (fields["city"] == ""){
      this.handleClasses(fields["city"], 8);
      error["city"] = false;
      msg = "Please fill required fields";
    }

    console.log(error)
    if (!error["password"] || !error["confirm_password"] || !error["email"] ||  !error["confirm_email"] || 
        !error["first_name"] || !error["last_name"] || !error["mobile_number"] || !error["country"] ||
        !error["country"] || !error["city"]){
      return {result : false, msg : msg};
    }
    return {result : true, msg : msg};
  }

  async handleSubmit(event) {
    event.preventDefault();
      this.setState({errorFields: {username: true, password : true, confirm_password : true, confirm_email : true,
                    email: true, first_name : true, last_name : true, mobile_number: true, gender: true, country : true, city : true }})
    let valid = this.validateFields(this.state.fields);
    if (valid.result && this.state.privacy){
      let form = this.state.fields;
      let fields = { last_name : form["last_name"], first_name : form["first_name"], email : form["email"], password : form["password"], gender : form["gender"], 
          contact_number : form["mobile_number"], country : form["country"], city : form["city"]  };
      // let data = jsonToURI(fields);
    let res = {};
    
      res = await apiModule('authentication_register',{}, fields);
    // }
    if(res.result == 'success'){
        window.confirm(res.message);
        this.props.history.push("/");
      }else{
        window.confirm('Error on signup. Your email address is already taken!');
      }
    

    }else{
      if(!this.state.privacy && valid.result == true){
        alert('Please read and accept the Terms & Conditions and Privacy Policy of Salto');
      }else{
        alert(valid.msg)
      }
    }
  }

  openDropDown(open) {
    if(open)
    this.setState( {openList : open, dropClass :"wrap-input100 validate-input m-b-50 drop-icon"} );
    else
    this.setState( {openList : open, dropClass :"wrap-input100 validate-input m-b-50 drop-icon-hide"} );
  }

  handleMale (toggle) {
    let fields = this.state.fields;
    fields["male"] = !this.state.fields["male"];
    fields["female"] = false;
    fields["gender"] = fields["male"] == true ? 'M' : '';
    this.setState({fields})
  }

  handleFemale (toggle) {
    let fields = this.state.fields;
    fields["female"] = !this.state.fields["female"];
    fields["male"] = false;
    fields["gender"] = fields["female"] == true ? 'F' : '';
    this.setState({fields})
  }

  render() {
    if(this.state.redirect){
      return (
        <Redirect to="/" />
        )
    }
    return (
     <div className="limiter"  style={styles.templateSkin}>
    <div className="container-login100">
      <div className="wrap-login100 p-t-85 p-b-20">
        <form onSubmit={this.handleSubmit} className="login100-form validate-form">
          <span className="login100-form-title p-b-70">
            Sign up
          </span>

          <div className={"wrap-input100 validate-input m-t-85 m-b-35 " + this.state.validate[3]} data-validate="Enter email">
            <input value={this.state.fields["email"]} onChange={event => {this.handleChanges(this, event); this.handleClasses(event.target.value, 3);}} className={"input100 " + this.state.classes[3]} type="text" name="email"/>
            <span className="focus-input100" data-placeholder="Email"></span>
          </div>

          <div className={"wrap-input100 validate-input m-b-35 " + this.state.validate[10]} data-validate="Same as email">
            <input value={this.state.fields["confirm_email"]} onChange={event => {this.handleChanges(this, event); this.handleClasses(event.target.value, 10);}} className={"input100 " + this.state.classes[10]} type="text" name="confirm_email"/>
            <span className="focus-input100" data-placeholder="Confirm Email"></span>
          </div> 
          <div className="m-b-50">
           <div className={"wrap-input100 validate-input " + this.state.validate[1]} data-validate="Enter password">
            <input value={this.state.fields["password"]} onChange={event => {this.handleChanges(this, event); this.handleClasses(event.target.value, 1);}} className={"input100 " + this.state.classes[1]} type="password" name="password"/>
            <span className="focus-input100" data-placeholder="Password"></span>

          </div>
            <span className="password-disclaimer">Use 8 or more characters with a mix of letters, numbers & symbols</span>
            </div>


          <div className={"wrap-input100 validate-input m-b-50 " + this.state.validate[2]} data-validate="Enter password">
            <input value={this.state.fields["confirm_password"]} onChange={event => {this.handleChanges(this, event); this.handleClasses(event.target.value, 2);}} className={"input100 " + this.state.classes[2]} type="password" name="confirm_password"/>
            <span className="focus-input100" data-placeholder="Confirm Password"></span>
          </div>

          <div className={"wrap-input100 validate-input m-b-50 " + this.state.validate[4]} data-validate="Enter first name">
            <input value={this.state.fields["first_name"]} onChange={event => {this.handleChanges(this, event); this.handleClasses(event.target.value, 4);}} className={"input100 " + this.state.classes[4]} type="text" name="first_name"/>
            <span className="focus-input100" data-placeholder="First Name"></span>
          </div>

          <div className={"wrap-input100 validate-input m-b-50 " + this.state.validate[5]} data-validate="Enter last name">
            <input value={this.state.fields["last_name"]} onChange={event => {this.handleChanges(this, event); this.handleClasses(event.target.value, 5);}} className={"input100 " + this.state.classes[5]} type="text" name="last_name"/>
            <span className="focus-input100" data-placeholder="Last Name"></span>
          </div>

          <div className={"wrap-input100 validate-input m-b-50 " + this.state.validate[6]} data-validate="Enter mobile number">
            <input value={this.state.fields["mobile_number"]} onChange={event => {this.handleChanges(this, event); this.handleClasses(event.target.value, 6);}} className={"input100 " + this.state.classes[6]} type="number" name="mobile_number"/>
            <span className="focus-input100" data-placeholder="Mobile Number"></span>
          </div>

          <div className="row m-b-30 red-border">
            <div className="col-xs-4"><label className="form-check-label" >Gender</label></div>
            <div className="col-xs-4 " >
                <input type="radio" checked={this.state.fields["male"]} onClick={event => {this.handleMale(event);}} className="form-check-input" id="male" name="male"/>
                <label className="form-check-label" htmlFor="materialChecked">Male</label>
            </div>
             <div className="col-xs-4">
                <input type="radio" checked={this.state.fields["female"]} onClick={event => {this.handleFemale(event);}} className="form-check-input" id="female" name="female"/>
                <label className="form-check-label" htmlFor="materialChecked2">Female</label>
            </div>
          </div>
          <div className={"wrap-input100 validate-input m-b-50 " + this.state.validate[7]} data-validate="Enter country">
            <input value={this.state.fields["country"]} onChange={event => {this.handleChanges(this, event); this.handleClasses(event.target.value, 7);}} className={"input100 " + this.state.classes[7]} type="text" name="country"/>
            <span className="focus-input100" data-placeholder="Country"></span>
          </div>

          <div className={"wrap-input100 validate-input m-b-50 " + this.state.validate[8]} data-validate="Enter city">
            <input value={this.state.fields["city"]} onChange={event => {this.handleChanges(this, event); this.handleClasses(event.target.value, 8);}} className={"input100 " + this.state.classes[8]} type="text" name="city"/>
            <span className="focus-input100" data-placeholder="City"></span>
          </div>  
  
          <div className="row m-b-50 m-b-30 red-border privacy-wrap" >
            <input type="checkbox" checked={this.state.privacy} onChange={event => {this.setState({privacy : !this.state.privacy});}} className="form-check-input" id="male" name="male"/>
            <label onClick={() => window.open(myConfig.PRIVACY_URL)} className="form-check-label" htmlFor="materialChecked"><u>I agree to the terms & conditions and privacy policy</u></label>
          </div>
          <div className="container-login100-form-btn">
            <button className="login100-form-btn" style={styles.templateTiles}>
              SUBMIT
            </button>
          </div>
          <div className="center-align">
            <label className="regular-label">Already a member? <u onClick={() => this.props.changePageHandler("Login")}><Link style={{ textDecoration: 'none', color: '#000000' }} to="/">Login</Link></u></label>
          </div>
        </form>
      </div>
    </div>
  </div>

    );
  }
}

export default withRouter(SignUp);