import React, { Component } from 'react';
import '../App.css';
import { writeData, readAllData, clearAllData, passwordFormat, jsonToURI } from '../commons/utility.js';
import { Loader } from '../commons/utilityViews.js';
import Aux from './aux.js';
import { myConfig } from '../commons/config.js';
import { apiModule } from '../commons/apiCall.js';
import queryString  from 'query-string';
import { withRouter } from 'react-router-dom';
import { styles } from '../styles/styles.js';
class ResetPassword extends Component {

  constructor(props) {
    super(props);
    this.state = {
      openList : false,
      listOfFarms: [],
      selectedFarm : '--',
      isLoading: false,
      email: '',
      password: '',
      confirm_password: ''
    };
    this.handleChanges = this.handleChanges.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChanges (e) {       
    if(e.target.name == 'password'){
      if(e != "" && e != undefined){
        this.state.classPassword = "has-val";
        this.state.passwordValidation = "";
      }else{
        this.state.classPassword = "";
        this.state.passwordValidation = "alert-validate";
      }
    }

    if(e.target.name == 'confirm_password'){
      if(e != "" && e != undefined){
        this.state.classConfirm = "has-val";
      this.state.confirmPasswordValidation = ""
      }else{
       this.state.classConfirm = "";
       this.state.confirmPasswordValidation = "alert-validate";
      }
    }
    this.setState({[e.target.name] : e.target.value});
  }

  handleValidation() {
    let bool = true;
    if(this.state.password == '' || this.state.confirm_password == ''){
      this.setState({passwordValidation : 'alert-validate', confirmPasswordValidation : 'alert-validate' });
      bool = false;
    }
    if (this.state.password.length < 8 ){
      bool = false;
      this.setState({passwordValidation : 'alert-validate'  });
    }
    if (this.state.confirm_password.length < 8 ){
      this.setState({confirmPasswordValidation : 'alert-validate'  });
      bool = false;
    }
    if(this.state.password != this.state.confirm_password){
      bool = false;
    }
    if(!passwordFormat(this.state.password)){
      bool = false;
    }
    if(!passwordFormat(this.state.confirm_password)){
      bool = false;
    }
    return bool;
  }

  async handleSubmit(event) {
   event.preventDefault();
    if(this.handleValidation()){
      const that = this;
      const parsed = queryString.parseUrl(window.location.href);
      let resetData = { token : parsed.query.token, password : this.state.password };
      let data = await apiModule('authentication_reset_password', {}, resetData);
      if(data.status == 'success'){
       window.confirm(data.message);
       window.location = parsed.url;
      } else {
        window.confirm(data.message);
      }
    }else{
      alert("Please input valid password.")
    }
  }

  render() {
    return (
      <div className="limiter"  style={styles.templateSkin}>
        <div className="container-login100">
          <div className="wrap-login100 p-t-85 p-b-20">
            <div className="login100-form validate-form">
            <form onSubmit={this.handleSubmit}>
            <span className="reset-form-title p-b-70">
                Please input new password
              </span>
              <div className={"wrap-input100 validate-input m-b-50 " + this.state.passwordValidation} data-validate="Enter password">
                <input value={this.state.password} onChange={event => {this.handleChanges(event); }} className={"input100 " + this.state.classPassword} type="password" name="password"/>
                <span className="focus-input100" data-placeholder="Password"></span>
              </div>

              <div className={"wrap-input100 validate-input m-b-50 " + this.state.confirmPasswordValidation} data-validate="Enter password">
                <input value={this.state.confirm_password} onChange={event => {this.handleChanges(event); }} className={"input100 " + this.state.classConfirm} type="password" name="confirm_password"/>
                <span className="focus-input100" data-placeholder="Confirm Password"></span>
              </div>
              <div className="container-login100-form-btn">
                <button type="submit" className="login100-form-btn" style={styles.templateTiles}>
                  SUBMIT
                </button>
              </div>
              <div className="center-align">
                <label className="regular-label">Already a member? <u onClick={() => this.props.history.push("/")}>Login</u></label>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
    
  }
}

export default withRouter(ResetPassword);