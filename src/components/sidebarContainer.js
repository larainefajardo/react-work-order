import React, { Component } from 'react';
import '../App.css';
import { writeData, clearAllData, prettifyDate, formatDateByNumber } from '../commons/utility';
import { Modal, Loader } from '../commons/utilityViews.js';
import Sidebar from './sidebar2';
import { menuConfig } from '../commons/menu.js';
import '../home.css';
import DatePicker from 'react-date-picker';
import {Doughnut} from 'react-chartjs-2';
import { myConfig } from '../commons/config';
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import { withRouter, Route } from 'react-router-dom';
import 'react-vertical-timeline-component/style.min.css';

class SidebarContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      toggle : undefined,
      currentSeasonTarget: '',
      openSeason : false,
      hideSeason: 'season-hide',
      seasonArrow : 'fa-angle-down',
      isLoading: false,
      menuIndex: "HOME",
      sidebar : undefined
    };
  }

async componentDidMount(){
  let jwt = this.props.getSession().jwt;
  let menu = this.props.match.params.name.toUpperCase().replace('-', ' ');
  this.setState({jwt : jwt, menuIndex : menu});
}

changeIndex = (index) => {
  this.setState({menuIndex : index});
}

toggleDrawer(){
  let drawer = !this.state.sidebar;
  this.setState({sidebar : drawer});
}

render() {
  myConfig.sidebarContext = this;
  return (
    <Sidebar toggle={this.state.sidebar}  setUserSession={(user) => this.props.setUserSession(user)} 
            signout={this.props.auth.signout} toggleSidebar={() => this.toggleDrawer()} getSession = {() => this.props.getSession()} parent={this}>
      {this.props.menuRoute(this)}
     </Sidebar>
  );
}
}

export default withRouter(SidebarContainer);