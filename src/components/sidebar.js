import React, { Component } from 'react';
import '../App.css';
import { writeData, passwordFormat, clearAllData } from '../commons/utility.js';
import { apiModule } from '../commons/apiCall.js';
import { menuConfig } from '../commons/menu.js';
import Aux from './aux.js';
import { styles } from '../styles/styles.js';
import { BrowserRouter as Router, Route, Link, withRouter, Redirect } from 'react-router-dom';
import { myConfig } from '../commons/config.js';
import { urlBase64ToUint8Array, jsonToURI } from '../commons/utility.js';

class Sidebar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            mainContainer : "main-container remove-overflow-mobile",
            sidebarWrapper : "sidebar closed",
            permissions : []
        };
    }

    async componentDidMount(){
        let jwt = this.props.getSession().jwt;
        let permissions = this.props.getSession().user.role.permissions;
        this.setState({jwt : jwt, permissions : permissions});
        let enableNotificationsButton = document.querySelectorAll('.enable-notifications');
        if('Notification' in window) {
          for(var i = 0; i < enableNotificationsButton.length;i++) {
            enableNotificationsButton[i].style.display = 'inline-block';
          }
        }else {
            //do nothing
        }
    }


    removeData = async () => {
            let user = {
                jwt: '',
                full_name: '',
                user: null
                
            }
        await clearAllData('users'); 
        this.props.setUserSession(user); 
        this.props.history.push({pathname : '/'});
    }
    displayConfirmNotification = () => {
        

        if('serviceWorker' in navigator) {
            var options = {
                body: 'You successfully subscribed to our Notification service!',
                icon: 'icons/icon-96x96.png',
                image: 'icons/icon-96x96.png',
                lang: 'en-US', //BCP 47
                vibrate: [100, 50, 200], //vibration pause vibration pause..
                badge: 'icons/icon-96x96.png',
                dir: 'ltr',
                tag: 'confirm-notification',
                renotify: true,
                actions : [
                    {action: 'confirm', title: 'Okay'},
                    {action: 'cancel', title: 'Cancel'}
                ]
            };
            navigator.serviceWorker.ready
                .then(function(swreg) {
                    swreg.showNotification('Successfully subscribed', options)
                });
        }
    }

    configurePushSub = () => {
        let that = this;
        console.log('configurePushSub ServiceWorker');
        if (!('serviceWorker' in navigator)) {
            console.log('No ServiceWorker');
            return;
        }

        if('PushManager' in window) {
            console.log('We have a push manager');

            var reg;
            navigator.serviceWorker.ready
            .then(function(swreg) {
                reg = swreg;
                console.log('navigator.serviceWorker.ready');
                console.log(swreg);
                let pushManager = swreg.pushManager;
                console.log(pushManager);
                
                return pushManager.getSubscription();
            })
            .then(function(sub) {
                if (sub === null) {
                // Create a new subscription
                var vapidPublicKey = myConfig.WEB_PUSH_PUBLIC_KEY;
                var convertedVapidPublicKey = urlBase64ToUint8Array(vapidPublicKey);
                console.log('Create a new subscription ' + myConfig.WEB_PUSH_PUBLIC_KEY);
                return reg.pushManager.subscribe({
                    userVisibleOnly: true,
                    applicationServerKey: convertedVapidPublicKey
                });
                } else {
                // We have a subscription
                console.log('We have a subscription');
                console.log(sub);
                return sub;
                }
            })
            .then(function(newSub) {
                let webpushObject = {
                    'webpush': JSON.stringify(newSub),
                    'type': 'default_push_type'
                };

                that.displayConfirmNotification(); 
                console.log(newSub);
                let request = {x_auth : that.props.getSession().jwt}
                return apiModule('user_store_vapid', request ,webpushObject);

                
            })
            .then(function(res) {
                console.log(res);
            })
            .catch(function(err) {
                console.log(err);
            });
        }else {
            
            
        }
    
        
    }
    
    callbackSidebar = (permissionData) => {
        let that = this;
        if(permissionData.permission === "granted") {

            let webpushObject = {
                'webpush': permissionData.deviceToken,
                'type': 'safari_push_type'
            };
    
            let data = jsonToURI(webpushObject);
            
            return apiModule('user_store_vapid', that.props.getSession().jwt ,data);
        }
    }
    
    askForNotificationPermission = () => {
        let that = this;
        
        if('PushManager' in window) {
            Notification.requestPermission(function(result) {
                console.log('User Choice', result);
                if(result !== 'granted') {
                    console.log('Not granted');
                }else {
                    that.configurePushSub();          
                }
            });
        }else {
            console.log('Sorry no push manager. Trying safari server...');
           
            let result = window.safari.pushNotification.permission(
                myConfig.SAFARI_WEB_PUSH_ID
            );
            
            if(result.permission === "default" || result.permission === "denied") {
                window.safari.pushNotification.requestPermission(myConfig.SERVER_URL,myConfig.SAFARI_WEB_PUSH_ID,
                {},that.callbackSidebar);
            }else {
                console.log(result.permission);
            }
            console.log(result);
        }   
    }

    hasAccess = (content) => {
      let found = content.some(r=> this.state.permissions.includes(r))
      return found;
    }

    sortByPageID(content){
        content.sort(( a, b ) => {
            return a.pageId < b.pageId ? -1 : a.pageId > b.pageId ? 1 : 0;
        });
    }
    
    sidebarCategory = (content) => {
    this.sortByPageID(content);
    return content.map((item, i) => {
        if(this.hasAccess(item.permission)){
            let index = i;
            let url = item.pageLabel.toLowerCase().replace(/ /g, '-');
            return (<Link style={{ textDecoration: 'none', color: '#fff' }} to={`/dashboard/${url}`}>
                <label key={i} onClick={() => { this.props.toggleSidebar(); }} 
                    className={"sidebar-label label-align " + item.isActive}>{item.pageLabel}</label></Link>
            );
        }
    })
    }

    render() {

        let mainContainer = "main-container remove-overflow-mobile";
        let sidebarWrapper = "sidebar closed";
        if(this.props.toggle == undefined){
            mainContainer = "main-container remove-overflow-mobile";
            sidebarWrapper = "sidebar closed";
        }else if(this.props.toggle){
            mainContainer = "main-container  remove-overflow-mobile";
            sidebarWrapper = "sidebar sidebar-opened";
        }else{
            mainContainer = "main-container remove-overflow-mobile";
            sidebarWrapper = "sidebar sidebar-closed";
        }
        
        return(
            <div className="containing">
                <div  className={mainContainer}>
                    <div className={sidebarWrapper}>
                        <div className="sidebar-content-wrapper" style={styles.templateTiles}>
                        <div className="profile">
                            <div>
                                <img className="img-card"  src={require('../assets/icons/person-silhouette.png')} /> 
                            </div>
                            <div className="sidebar-profile">
                                <label className="sidebar-label-farm">{this.props.getSession().full_name}</label>
                                <label className="sidebar-label-email">{this.props.getSession().user.email}</label>
                                <label onClick={() => this.props.history.push({pathname : "/profile"})} className="sidebar-view-profile">View Profile</label>
                            </div>
                        </div>  
                            <div className="breeder-container">
                                {this.sidebarCategory(menuConfig)}    
                            </div>
                            <div className="sidebar-logout">
                                <div onClick={() =>  this.askForNotificationPermission()} className="menu-wrap">
                                    <label className="sidebar-label logout-contain">Allow Push</label>
                                    <img className="menu-bar-logout" src={require('../assets/icons/alert.svg')} />
                                </div>
                                <div onClick={() =>  this.removeData()} className="menu-wrap">
                                    <label className="sidebar-label logout-contain">Logout</label>
                                    <img className="menu-bar-logout" src={require('../assets/icons/logout.svg')} />
                                </div>
                            </div>
                        </div>
                        </div>
                    {this.props.children}
                </div>
            </div>

        );
    }
}
   
export default withRouter(Sidebar);


