import React, { Component } from 'react';
import '../App.css';
import { writeData, readAllData, clearAllData, jsonToURI } from '../commons/utility.js';
import Aux from './aux.js';
import { myConfig } from '../commons/config.js';
import { apiModule } from '../commons/apiCall.js';
import { withRouter } from 'react-router-dom';
import { styles } from '../styles/styles.js';
class ForgotPassword extends Component {

  constructor(props) {
    super(props);
    this.state = {
      openList : false,
      listOfFarms: [],
      selectedFarm : '--',
      isLoading: false,
      email: ''
    };
    this.handleChanges = this.handleChanges.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChanges (e) {       
    if(e != "" && e != undefined){
      this.state.classes = "has-val";
      this.state.validate = "";
    }else{
       this.state.classes = "";
       this.state.validate = "alert-validate";
    }
    this.setState({[e.target.name] : e.target.value});
  }

  handleValidation(email_value) {
    let email = true;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(email_value == ""){
      this.setState( {validate : " alert-validate" ,
                          emailMessage : "Enter valid email"} );
      email = false;
    }

    if(!re.test(email_value.toLowerCase())){
      
      this.setState( {validate : " alert-validate" ,
                          emailMessage : "Enter valid email"} );
      email = false;

    }

    return email;
  }

  async handleSubmit(event) {
   event.preventDefault();
    if(this.handleValidation(this.state.email)){
      const that = this;
      let item = { email : this.state.email };
      // let postData = jsonToURI(item);
      let data = await apiModule('authentication_forgot_password', {}, item);
        if(data.status == 'success'){
         window.confirm(data.message);
         that.props.history.push("/");
        } else {
          window.confirm(JSON.stringify(data));
        }
    }else{
      window.confirm("Please enter an email.")
    }
  }

  render() {
    return (
     <div className="limiter"  style={styles.templateSkin}>
    <div className="container-login100">
      <div className="wrap-login100 p-t-85 p-b-20">
        <div className="login100-form validate-form">
        <form onSubmit={this.handleSubmit}>
         <span className="reset-form-title p-b-70">
           Please input email address to reset
          </span>
         <div className={"wrap-input100 validate-input m-b-50 " + this.state.validate} data-validate="Enter valid email">
            <input value={this.state.email} onChange={event => {this.handleChanges(event);}} className={"input100 " + this.state.classes} type="text" name="email"/>
            <span className="focus-input100" data-placeholder="Email address"></span>
          </div>    
          <div className="container-login100-form-btn">
            <button type="submit" className="login100-form-btn" style={styles.templateTiles}>
              SUBMIT
            </button>
          </div>
          <div className="center-align">
            <label className="regular-label">Already a member? <u onClick={() => this.props.history.goBack()}>Login</u></label>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
    );
    
  }
}


export default withRouter(ForgotPassword);