import React, { Component } from 'react';
import '../App.css';
import { writeData, jsonToURI } from '../commons/utility.js';
import { styles } from '../styles/styles.js';
import Aux from './aux.js';
import { Modal, Loader } from '../commons/utilityViews.js';
import { myConfig } from '../commons/config.js';
import { BrowserRouter as Router, Route, Link, Redirect, withRouter } from 'react-router-dom';
import { apiModule } from '../commons/apiCall.js';
import GoogleLogin from 'react-google-login';

const responseGoogle = (response) => {
  console.log(response);
}
class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '', 
      password : '', 
      dbP: undefined, 
      isLoading: false, 
      emailClasses : "input100", 
      passClasses : "input100",
      emailValidation : "wrap-input100 validate-input m-t-85 m-b-35",
      passwordValidation : "wrap-input100 validate-input m-b-50",
      emailMessage: "Enter email",
      passwordMessage: "Enter password",
  };
    this.FB = window.FB;
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePass = this.handleChangePass.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount(){
    let that = this;
    try{
    window.FB.XFBML.parse();
    console.log(this.props.auth.isAuthenticated)
    this.props.auth.signout();
    window.FB.Event.subscribe('auth.statusChange', (response) => {
    if (response.status === "connected") {
      console.log(response)
      that.setState({isLoading : true});
      window.FB.api('/me', {fields: 'email,name,picture'}, userData => {
        let registerUser = {thirdparty_id : userData.id, thirdpart_source : 'facebook', email : userData.email, password : 'fb_login', full_name : userData.name};
        apiModule('authentication_register',{}, registerUser)
          .then(() => {
          that.loginThirdParty(userData);
        })
      });
    } else {
      window.FB.login(response => {
        if (response.authSuccess) {
          // dispatch(oauth("facebook"));
          console.log(response)
        }
      }, { scope: "email,public_profile", info_fields: "email,name" });
    }
    });
  }catch(e){
    console.log(e);
  }

  }


  loginThirdParty = (userData) => {
    const that = this;
    let postData = {email : userData.email, password : 'fb_login'};
    apiModule('authentication_login', {}, postData)
    .then((data) => {
      if(data.result == 'success'){
        that.getSession(data);
      }else{
        this.setState({isLoading : false});
        window.confirm(data.message);
      }
    });
  }

   onSuccess(googleUser) {
    try{
        console.log('Logged in as: ' + window.googleUser.getBasicProfile().getName());
    }catch(err){
      console.log(err)
    }
      }
       onFailure(error) {
        console.log(error);
      }

  renderButton(){
    window.gapi.signin2.render('my-signin2', {
          'scope': 'profile email',
          'width': 240,
          'height': 50,
          'longtitle': true,
          'theme': 'dark',
          'onsuccess': this.onSuccess(),
          'onfailure': this.onFailure()
        });
  }

  handleChangeEmail (event) {
    this.setState( {email : event.target.value} );
    if(event.target.value != ""){
      this.setState({emailClasses : "input100 has-val",  emailMessage : "Enter valid email", emailValidation : "wrap-input100 validate-input m-t-85 m-b-35" })
    }else{
      this.setState({emailClasses : "input100", emailMessage : "Enter email", emailValidation : "wrap-input100 validate-input m-t-85 m-b-35 alert-validate"})
    }
  }

  handleChangePass (event) {
    this.setState( {password : event.target.value} );
    if(event.target.value != ""){
      this.setState({passClasses : "input100 has-val", passwordValidation : "wrap-input100 validate-input m-b-50"})
    }else{
      this.setState({passClasses : "input100", passwordMessage : "Enter password",
                    passwordValidation : "wrap-input100 validate-input m-b-50 alert-validate"})
    }
  }

  validateField(fieldName, value) {
  let email = true;
  let password = true;
  if(this.state.email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i) == false){
      this.setState( {emailValidation : "wrap-input100 validate-input m-t-85 m-b-35 alert-validate" ,
                        emailMessage : "Enter valid email"} );
    email = false
  }

  if(this.state.password == ""){
    password = false;
    
    this.setState({passwordValidation : "wrap-input100 validate-input m-b-50 alert-validate"})
  }

  if(this.state.email == ""){
    this.setState( {emailValidation : "wrap-input100 validate-input m-t-85 m-b-35 alert-validate" ,
                        emailMessage : "Enter valid email"} );
    email = false
  }

  return email == true && password == true ? true : false;
  }

  async handleSubmit(event) {
     event.preventDefault();
    let bool = this.validateField(event);  
    if (bool) {
      this.setState({isLoading : true});
    const that = this;
      let postData = {email : this.state.email, password : this.state.password};
      // let post = jsonToURI(postData);
      let data = await apiModule('authentication_login', {}, postData);
      if(data.result == 'success'){
        this.setState({isLoading : false});
        this.getSession(data);
      } else {
        this.setState({isLoading : false});
        if(data.result == 'expired_password'){
         that.props.changePageHandler("ResetPassword"); 
        }else{
           window.confirm(data.message);
        }
      }
  }
  }

getSession(data){
  const that = this;
  let result = {
      jwt : data.message,
      full_name: data.user.full_name,
      user : data.user,
      id: 1,
  };
  writeData('users', result);
  let user = {
    jwt: data.message,
    full_name: data.user.full_name,
    user: data.user
  }
   that.props.setUserSession(user);
   that.setState({isLoading : false})
  that.props.history.push({pathname : '/dashboard/home'});
}


  render() {
    
    return (
     <div className="limiter login-wrapper">
      <div className="container-login100">
        <div className="wrap-login100 p-t-85 p-b-20 logo-login">
          <img className="dice-login" src={require('../assets/icons/logo-white.png')} />
          <label className="work-login">WORK ORDER FORM</label>
        </div>
        <div className="line-login"></div>
        <div className="wrap-login100 p-t-85 p-b-20">
          <form onSubmit={this.handleSubmit} className="login100-form validate-form">
            <span className="login100-form-title login p-b-70">
              Sign In
            </span>

            <div className={this.state.emailValidation} data-validate = {this.state.emailMessage}>
              <input  value={this.state.email} onChange={event => this.handleChangeEmail(event)} className={this.state.emailClasses} type="text" name="email"/>
              <span className="focus-input100" data-placeholder="Username/Email"></span>
            </div>

            <div className={this.state.passwordValidation} data-validate={this.state.passwordMessage}>
              <input value={this.state.password} onChange={event => this.handleChangePass(event)} className={this.state.passClasses} type="password" name="pass"/>
              <span className="focus-input100" data-placeholder="Password"></span>
            </div>

            <div className="container-login100-form-btn">
              <button className="login100-form-btn">
                Sign In
              </button>
            </div>
              <label className="regular-label"><u onClick={() => this.props.changePageHandler("ForgotPassword")} ><Link style={{ textDecoration: 'none', color: '#fff' }} to="/forgot-password">Forgot Password?</Link></u></label>
            <div className="center-align-signup">
              <label className="regular-label">Not yet a member? <u onClick={() => this.props.changePageHandler("Sign Up")}><Link to="/register" style={{ textDecoration: 'none', color: '#000000' }}>Sign up</Link></u></label>
            </div>
          </form>
        </div>
    </div>
    <Loader show={this.state.isLoading} />
  </div>

    );
  }
}

export default withRouter(Login);