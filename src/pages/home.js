import React, { Component } from 'react';
import '../App.css';
import { writeData, clearAllData, prettifyDate, formatDateByNumber } from '../commons/utility';
import { Modal, Loader } from '../commons/utilityViews.js';
import Sidebar from '../components/sidebar';
import '../home.css';
import DatePicker from 'react-date-picker';
import {Doughnut} from 'react-chartjs-2';
import { myConfig } from '../commons/config';
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import { withRouter } from 'react-router-dom';
import 'react-vertical-timeline-component/style.min.css';

class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      toggle : undefined,
      toggleList: 'sidebar-close',
      targetHatch : new Date(),
      seasonName : '',
      allSeason: undefined,
      currentSeason: '-- --',
      currentSeasonTarget: '',
      openSeason : false,
      hideSeason: 'season-hide',
      seasonArrow : 'fa-angle-down',
      isLoading: false,
      menuIndex: 0,
      activeMenu: ['active','','','','',''],
      
      values: [Math.random() * 10000, Math.random() * 10000,
            Math.random() * 10000, Math.random() * 10000,
            Math.random() * 10000],

      labels: ['Elves', 'Dwarves',
                    'Hobbitses', 'Men', 'Wizards']
    
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }



async componentDidMount(){
  let jwt = this.props.getSession().jwt;
  this.setState({jwt : jwt});
  
}


toggleList (toggle) {
  return toggle == 'sidebar-close' ? 'sidebar-open' : 'sidebar-close';
}

async logOut(){
  clearAllData('users');
  let user = {
            jwt: '',
            full_name: '',
            user: null
        }
  window.location= myConfig.SITE_URL;
}

showModal = () => {
  this.setState({ show: true });
}
  
hideModal = () => {
  this.setState({ show: false });
}

onChange = value => this.setState({ targetHatch : value })

handleSubmit = (e) => {
  e.preventDefault()
  
}

handleChanges (e) {    
 
}


render() {
  if(this.state.isLoading){
    return(
      <Loader handleClose={this.hideModal} show={this.state.isLoading} />
      )
  }else{
    return (
        <div className="k-sweetalert2--nopadding k-header--static k-header-mobile--fixed k-subheader--enabled k-subheader--transparent k-aside--enabled k-aside--fixed k-page--loading">

          <div id="k_header_mobile" className="k-header-mobile  k-header-mobile--fixed ">
              <div className="k-header-mobile__logo">
                  <a href="index.html">
                      <img alt="Logo" src={require( "../assets/media/logos/logo-1.png")}/>
                  </a>
              </div>
              <div className="k-header-mobile__toolbar">
                  <button className="k-header-mobile__toolbar-toggler k-header-mobile__toolbar-toggler--left" id="k_aside_mobile_toggler"><span></span></button>

                  <button className="k-header-mobile__toolbar-topbar-toggler" id="k_header_mobile_topbar_toggler"><i className="flaticon-more"></i></button>
              </div>
          </div>
          <div class="k-grid k-grid--hor k-grid--root">
              <div class="k-grid__item k-grid__item--fluid k-grid k-grid--ver k-page">

                  <button className="k-aside-close " id="k_aside_close_btn"><i className="la la-close"></i></button>

                  <div className="k-aside  k-aside--fixed  k-grid__item k-grid k-grid--desktop k-grid--hor-desktop" id="k_aside">

                      <div className="k-aside__brand   k-grid__item" id="k_aside_brand">
                          <div className="k-aside__brand-logo">
                              <a href="index.html">
                                  <img alt="Logo" src={require( "../assets/media/logos/logo-1.png")}/>
                              </a>
                          </div>
                          <div className="k-aside__brand-tools">
                              <button className="k-aside__brand-aside-toggler k-aside__brand-aside-toggler--left " id="k_aside_toggler"><span></span></button>
                          </div>
                      </div>

                      <div className="k-aside-menu-wrapper k-grid__item k-grid__item--fluid" id="k_aside_menu_wrapper">
                          <div id="k_aside_menu" className="k-aside-menu " data-kmenu-vertical="1" data-kmenu-scroll="1" data-kmenu-dropdown-timeout="500">
                              <ul className="k-menu__nav ">
                                  <li className="k-menu__item  k-menu__item--submenu k-menu__item--open k-menu__item--here" aria-haspopup="true" data-kmenu-submenu-toggle="hover">
                                      <a href="javascript:;" className="k-menu__link k-menu__toggle">
                                          <i className="k-menu__link-icon fa fa-chart-bar"></i>
                                          <span className="k-menu__link-text">Dashboards
                                            </span>

                                      </a>
                                  </li>
                                  <li className="k-menu__item " aria-haspopup="true">
                                      <a href="components/calendar/basic.html" className="k-menu__link ">
                                          <i className="k-menu__link-icon flaticon2-calendar-2"></i>
                                          <span className="k-menu__link-text">Calendar
                                        </span>
                                      </a>
                                  </li>

                                  <li className="k-menu__item  k-menu__item--submenu k-menu__item--open" aria-haspopup="true" data-kmenu-submenu-toggle="hover">
                                      <a href="#" className="k-menu__link k-menu__toggle">
                                          <i className="k-menu__link-icon flaticon2-copy"></i>
                                          <span className="k-menu__link-text">Request Work</span>
                                          <i className="k-menu__ver-arrow la la-angle-right">
                                        </i>
                                      </a>
                                      <div className="k-menu__submenu ">
                                          <span className="k-menu__arrow">
                                            </span>
                                          <ul className="k-menu__subnav">
                                              <li className="k-menu__item  k-menu__item--active" aria-haspopup="true">
                                                  <a href="components/forms/forms.html" className="k-menu__link ">
                                                      <i className="k-menu__link-bullet k-menu__link-bullet--dot">
                                                                <span></span>
                                                            </i>
                                                      <span className="k-menu__link-text">Creative
                                                                </span>
                                                  </a>
                                              </li>
                                              <li className="k-menu__item  k-menu__item" aria-haspopup="true">
                                                  <a href="index.html" className="k-menu__link ">
                                                      <i className="k-menu__link-bullet k-menu__link-bullet--dot">
                                                                <span></span>
                                                            </i>
                                                      <span className="k-menu__link-text">Developers
                                                                </span>
                                                  </a>
                                              </li>
                                              <li className="k-menu__item " aria-haspopup="true">
                                                  <a href="#" className="k-menu__link ">
                                                      <i className="k-menu__link-bullet k-menu__link-bullet--dot">
                                                            <span></span>
                                                            </i>
                                                      <span className="k-menu__link-text">QA
                                                            </span>
                                                  </a>
                                              </li>
                                              <li className="k-menu__item " aria-haspopup="true">
                                                  <a href="#l" className="k-menu__link ">
                                                      <i className="k-menu__link-bullet k-menu__link-bullet--dot">
                                                                <span></span></i>
                                                      <span className="k-menu__link-text">Others</span>
                                                  </a>
                                              </li>
                                          </ul>
                                      </div>
                                  </li>
                                  <li className="k-menu__item  k-menu__item--submenu" aria-haspopup="true" data-kmenu-submenu-toggle="hover">
                                      <a href="javascript:;" className="k-menu__link k-menu__toggle">
                                          <i className="k-menu__link-icon flaticon2-layers-1 "></i>
                                          <span className="k-menu__link-text">Upload Files</span>

                                      </a>
                                  </li>
                                  <li className="k-menu__item  k-menu__item--submenu" aria-haspopup="true" data-kmenu-submenu-toggle="hover">
                                      <a href="components/project list/list.html" className="k-menu__link k-menu__toggle">
                                          <i className="k-menu__link-icon flaticon2-list-3"></i>
                                          <span className="k-menu__link-text">Project List</span>

                                      </a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>

                  <div className="k-grid__item k-grid__item--fluid k-grid k-grid--hor k-wrapper">

                      <div id="k_header" className="k-header k-grid__item " data-kheader-minimize="on">

                          <div className="k-subheader   k-grid__item" id="k_subheader">
                              <div className="k-subheader__main">
                                  <h3 className="k-subheader__title">Work Order Form</h3>

                                  <span className="k-subheader__separator k-hidden"></span>
                                  <div className="k-subheader__breadcrumbs">
                                      <a href="index.html#" className="k-subheader__breadcrumbs-home"><i className="flaticon2-shelter"></i></a>
                                      <span className="k-subheader__breadcrumbs-separator"></span>
                                      <a href="index.html" className="k-subheader__breadcrumbs-link">
                                    Dashboards                    </a>

                                  </div>

                              </div>
                          </div>

                          <div className="k-header__topbar">
                              <div className="k-header__topbar-item k-header__topbar-item--search">
                                  <div className="k-header__topbar-wrapper">
                                      <div className="k-quick-search k-quick-search--inline k-quick-search--result-compact" id="k_quick_search_inline">
                                          <form method="get" className="k-quick-search__form">
                                              <div className="input-group">
                                                  <div className="input-group-prepend"><span className="input-group-text"><i className="flaticon2-search-1"></i></span></div>
                                                  <input type="text" className="form-control k-quick-search__input" placeholder="Search..." />
                                                  <div className="input-group-append"><span className="input-group-text"><i className="la la-close k-quick-search__close"></i></span></div>
                                              </div>
                                          </form>

                                          <div data-toggle="dropdown" data-offset="0,15px"></div>

                                          <div className="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
                                              <div className="k-quick-search__wrapper k-scroll" data-scroll="true" data-height="325" data-mobile-height="200">
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div className="k-header__topbar-item k-header__topbar-item--search k-hidden">
                                  <div className="k-input-icon k-input-icon--right">
                                      <input type="text" className="form-control" placeholder="Search..." />
                                      <span className="k-input-icon__icon k-input-icon__icon--right">
                                        <span><i className="la la-search"></i></span>
                                      </span>
                                  </div>
                              </div>

                              <div className="k-header__topbar-item" data-toggle="k-tooltip" title="Quick panel" data-placement="right">
                                  <div className="k-header__topbar-wrapper">
                                      <span className="k-header__topbar-icon" id="k_quick_panel_toggler_btn"><i className="fa fa-copy"></i></span>
                                  </div>
                              </div>

                              <div className="k-header__topbar-item dropdown">
                                  <div className="k-header__topbar-wrapper" data-toggle="dropdown" data-offset="30px -2px">
                                      <span className="k-header__topbar-icon k-bg-brand"><i className="fa fa-bell k-font-light"></i></span>
                                      <span className="k-badge k-badge--danger k-badge--notify">3</span>
                                  </div>
                                  <div className="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
                                      <div className="k-head bg-bg">
                                          <h3 className="k-head__title">User Notifications</h3>
                                          <div className="k-head__sub"><span className="k-head__desc">23 new notifications</span></div>
                                      </div>
                                      <div className="k-notification k-margin-t-30 k-margin-b-20 k-scroll" data-scroll="true" data-height="270" data-mobile-height="220">
                                          <a href="index.html#" className="k-notification__item">
                                              <div className="k-notification__item-icon">
                                                  <i className="flaticon2-line-chart k-font-success"></i>
                                              </div>
                                              <div className="k-notification__item-details">
                                                  <div className="k-notification__item-title">
                                                      New order has been received
                                                  </div>
                                                  <div className="k-notification__item-time">
                                                      2 hrs ago
                                                  </div>
                                              </div>
                                          </a>
                                          <a href="index.html#" className="k-notification__item">
                                              <div className="k-notification__item-icon">
                                                  <i className="flaticon2-box-1 k-font-brand"></i>
                                              </div>
                                              <div className="k-notification__item-details">
                                                  <div className="k-notification__item-title">
                                                      New customer is registered
                                                  </div>
                                                  <div className="k-notification__item-time">
                                                      3 hrs ago
                                                  </div>
                                              </div>
                                          </a>
                                          <a href="index.html#" className="k-notification__item">
                                              <div className="k-notification__item-icon">
                                                  <i className="flaticon2-chart2 k-font-danger"></i>
                                              </div>
                                              <div className="k-notification__item-details">
                                                  <div className="k-notification__item-title">
                                                      Application has been approved
                                                  </div>
                                                  <div className="k-notification__item-time">
                                                      3 hrs ago
                                                  </div>
                                              </div>
                                          </a>
                                          <a href="index.html#" className="k-notification__item">
                                              <div className="k-notification__item-icon">
                                                  <i className="flaticon2-image-file k-font-warning"></i>
                                              </div>
                                              <div className="k-notification__item-details">
                                                  <div className="k-notification__item-title">
                                                      New file has been uploaded
                                                  </div>
                                                  <div className="k-notification__item-time">
                                                      5 hrs ago
                                                  </div>
                                              </div>
                                          </a>
                                          <a href="index.html#" className="k-notification__item">
                                              <div className="k-notification__item-icon">
                                                  <i className="flaticon2-bar-chart k-font-info"></i>
                                              </div>
                                              <div className="k-notification__item-details">
                                                  <div className="k-notification__item-title">
                                                      New user feedback received
                                                  </div>
                                                  <div className="k-notification__item-time">
                                                      8 hrs ago
                                                  </div>
                                              </div>
                                          </a>
                                          <a href="index.html#" className="k-notification__item">
                                              <div className="k-notification__item-icon">
                                                  <i className="flaticon2-pie-chart-2 k-font-success"></i>
                                              </div>
                                              <div className="k-notification__item-details">
                                                  <div className="k-notification__item-title">
                                                      System reboot has been successfully completed
                                                  </div>
                                                  <div className="k-notification__item-time">
                                                      12 hrs ago
                                                  </div>
                                              </div>
                                          </a>
                                          <a href="index.html#" className="k-notification__item">
                                              <div className="k-notification__item-icon">
                                                  <i className="flaticon2-favourite k-font-focus"></i>
                                              </div>
                                              <div className="k-notification__item-details">
                                                  <div className="k-notification__item-title">
                                                      New order has been placed
                                                  </div>
                                                  <div className="k-notification__item-time">
                                                      15 hrs ago
                                                  </div>
                                              </div>
                                          </a>
                                          <a href="index.html#" className="k-notification__item k-notification__item--read">
                                              <div className="k-notification__item-icon">
                                                  <i className="flaticon2-safe k-font-primary"></i>
                                              </div>
                                              <div className="k-notification__item-details">
                                                  <div className="k-notification__item-title">
                                                      Company meeting canceled
                                                  </div>
                                                  <div className="k-notification__item-time">
                                                      19 hrs ago
                                                  </div>
                                              </div>
                                          </a>
                                          <a href="index.html#" className="k-notification__item">
                                              <div className="k-notification__item-icon">
                                                  <i className="flaticon2-psd k-font-success"></i>
                                              </div>
                                              <div className="k-notification__item-details">
                                                  <div className="k-notification__item-title">
                                                      New report has been received
                                                  </div>
                                                  <div className="k-notification__item-time">
                                                      23 hrs ago
                                                  </div>
                                              </div>
                                          </a>
                                          <a href="index.html#" className="k-notification__item">
                                              <div className="k-notification__item-icon">
                                                  <i className="flaticon-download-1 k-font-danger"></i>
                                              </div>
                                              <div className="k-notification__item-details">
                                                  <div className="k-notification__item-title">
                                                      Finance report has been generated
                                                  </div>
                                                  <div className="k-notification__item-time">
                                                      25 hrs ago
                                                  </div>
                                              </div>
                                          </a>
                                          <a href="index.html#" className="k-notification__item">
                                              <div className="k-notification__item-icon">
                                                  <i className="flaticon-security k-font-warning"></i>
                                              </div>
                                              <div className="k-notification__item-details">
                                                  <div className="k-notification__item-title">
                                                      New customer comment recieved
                                                  </div>
                                                  <div className="k-notification__item-time">
                                                      2 days ago
                                                  </div>
                                              </div>
                                          </a>
                                          <a href="index.html#" className="k-notification__item">
                                              <div className="k-notification__item-icon">
                                                  <i className="flaticon2-pie-chart k-font-focus"></i>
                                              </div>
                                              <div className="k-notification__item-details">
                                                  <div className="k-notification__item-title">
                                                      New customer is registered
                                                  </div>
                                                  <div className="k-notification__item-time">
                                                      3 days ago
                                                  </div>
                                              </div>
                                          </a>
                                      </div>
                                  </div>
                              </div>

                              <div className="k-header__topbar-item k-header__topbar-item--user">
                                  <div className="k-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px -2px">
                                      <img alt="Pic" src={require( "../assets/media/users/300_21.jpg")}/>

                                  </div>
                                  <div className="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-md">
                                      <div className="k-user-card k-margin-b-50 k-margin-b-30-tablet-and-mobile bg-bg">
                                          <div className="k-user-card__wrapper">
                                              <div className="k-user-card__pic">
                                                  <img alt="Pic" src={require( "../assets/media/users/300_21.jpg")} />
                                              </div>
                                              <div className="k-user-card__details">
                                                  <div className="k-user-card__name">Alex Stone</div>
                                                  <div className="k-user-card__position">CTO, Loop Inc.</div>
                                              </div>
                                          </div>
                                      </div>

                                      <ul className="k-nav k-margin-b-10">
                                          <li className="k-nav__item">
                                              <a href="profile.html" className="k-nav__link">
                                                  <span className="k-nav__link-icon"><i className="flaticon2-calendar-3"></i></span>
                                                  <span className="k-nav__link-text">My Profile</span>
                                              </a>
                                          </li>
                                          <li className="k-nav__item">
                                              <a href="https://keenthemes.com/keen/preview/demo2/custom/user/profile-v1.html" className="k-nav__link">
                                                  <span className="k-nav__link-icon"><i className="flaticon2-browser-2"></i></span>
                                                  <span className="k-nav__link-text">My Tasks</span>
                                              </a>
                                          </li>
                                          <li className="k-nav__item">
                                              <a href="https://keenthemes.com/keen/preview/demo2/custom/user/profile-v1.html" className="k-nav__link">
                                                  <span className="k-nav__link-icon"><i className="flaticon2-mail"></i></span>
                                                  <span className="k-nav__link-text">Messages</span>
                                              </a>
                                          </li>
                                          <li className="k-nav__item">
                                              <a href="https://keenthemes.com/keen/preview/demo2/custom/user/profile-v1.html" className="k-nav__link">
                                                  <span className="k-nav__link-icon"><i className="flaticon2-gear"></i></span>
                                                  <span className="k-nav__link-text">Settings</span>
                                              </a>
                                          </li>
                                          <li className="k-nav__item k-nav__item--custom k-margin-t-15">
                                              <a href="https://keenthemes.com/keen/preview/demo2/custom/user/login-v2.html" target="_blank" className="btn btn-outline-metal btn-hover-brand btn-upper btn-font-dark btn-sm btn-bold">Sign Out</a>
                                          </li>
                                      </ul>
                                  </div>
                              </div>

                          </div>
                      </div>

                      <div className="k-grid__item k-grid__item--fluid k-grid k-grid--hor">

                          <div className="k-subheader   k-grid__item" id="k_subheader">
                              <div className="k-subheader__main">
                                  <h3 className="k-subheader__title">Dashboard</h3>

                                  <span className="k-subheader__separator k-hidden"></span>
                                  <div className="k-subheader__breadcrumbs">
                                      <a href="index.html#" className="k-subheader__breadcrumbs-home"><i className="flaticon2-shelter"></i></a>
                                      <span className="k-subheader__breadcrumbs-separator"></span>
                                      <a href="index.html" className="k-subheader__breadcrumbs-link">
                              Dashboards                    </a>
                                      <span className="k-subheader__breadcrumbs-separator"></span>
                                      <a href="index.html" className="k-subheader__breadcrumbs-link">
                              Brand Aside                    </a>

                                  </div>

                              </div>
                          </div>

                          <div className="k-content  k-grid__item k-grid__item--fluid" id="k_content">

                              <div className="row">
                                  <div className="col-lg-6 col-xl-6 order-lg-1 order-xl-1">

                                      <div className="k-portlet k-portlet--height-fluid k-widget-13">
                                          <div className="k-portlet__body">
                                              <div id="k-widget-slider-13-2" className="k-slider carousel slide" data-ride="carousel" data-interval="4000">
                                                  <div className="k-slider__head">
                                                      <div className="k-slider__label">Projects</div>
                                                      <div className="k-slider__nav">
                                                          <a className="k-slider__nav-prev carousel-control-prev" href="index.html#k-widget-slider-13-2" role="button" data-slide="prev">
                                                              <i className="fa fa-angle-left"></i>
                                                          </a>
                                                          <a className="k-slider__nav-next carousel-control-next" href="index.html#k-widget-slider-13-2" role="button" data-slide="next">
                                                              <i className="fa fa-angle-right"></i>
                                                          </a>
                                                      </div>
                                                  </div>
                                                  <div className="carousel-inner">
                                                      <div className="carousel-item k-slider__body active">
                                                          <div className="k-widget-13 k-widget-12">
                                                              <div className="k-widget-13__body row">
                                                                  <div className="col-8">
                                                                      <a className="k-widget-13__title" href="index.html#">Allianz Sales Portal</a>
                                                                  </div>
                                                                  <div className="col-4">
                                                                      <div className="k-widget-12__members">
                                                                          <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="John Myer">
                                                                              <img src={require( "../assets/media/users/100_1.jpg")} alt="image" />
                                                                          </a>
                                                                          <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="Alison Brandy">
                                                                              <img src={require( "../assets/media/users/100_10.jpg")} alt="image" />
                                                                          </a>
                                                                          <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="Selina Cranson">
                                                                              <img src={require( "../assets/media/users/100_1.jpg")} alt="image" />
                                                                          </a>
                                                                          <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="Luke Walls">
                                                                              <img src={require( "../assets/media/users/100_1.jpg")} alt="image" />
                                                                          </a>
                                                                          <a href="general.html#" className="k-widget-12__member k-widget-12__member--last">
                                                      +10
                                                  </a>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                              <div className="k-widget-13__foot">
                                                                  <div className="k-widget-13__progress">
                                                                      <div className="k-widget-13__progress-info">
                                                                          <div className="k-widget-13__progress-status">
                                                                              Progress
                                                                          </div>
                                                                          <div className="k-widget-13__progress-value">78%</div>
                                                                      </div>
                                                                      <div className="progress">
                                                                          <div className="progress-bar k-bg-brand w78" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100"></div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div className="carousel-item k-slider__body">
                                                          <div className="k-widget-13 k-widget-12">
                                                              <div className="k-widget-13__body row">
                                                                  <div className="col-8">
                                                                      <a className="k-widget-13__title" href="index.html#">Pediasure Animation</a>
                                                                  </div>
                                                                  <div className="col-4">
                                                                      <div className="k-widget-12__members">
                                                                          <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="John Myer">
                                                                              <img src={require( "../assets/media/users/100_1.jpg")} alt="image" />
                                                                          </a>
                                                                          <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="Alison Brandy">
                                                                              <img src={require( "../assets/media/users/100_10.jpg")} alt="image" />
                                                                          </a>
                                                                          <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="Selina Cranson">
                                                                              <img src={require( "../assets/media/users/100_1.jpg")} alt="image" />
                                                                          </a>
                                                                          <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="Luke Walls">
                                                                              <img src={require( "../assets/media/users/100_1.jpg")} alt="image" />
                                                                          </a>
                                                                          <a href="general.html#" className="k-widget-12__member k-widget-12__member--last">
                                                      +10
                                                  </a>
                                                                      </div>
                                                                  </div>

                                                              </div>
                                                              <div className="k-widget-13__foot">
                                                                  <div className="k-widget-13__progress">
                                                                      <div className="k-widget-13__progress-info">
                                                                          <div className="k-widget-13__progress-status">
                                                                              Progress
                                                                          </div>
                                                                          <div className="k-widget-13__progress-value">55%</div>
                                                                      </div>
                                                                      <div className="progress">
                                                                          <div className="progress-bar k-bg-brand w55" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div className="carousel-item k-slider__body">
                                                          <div className="k-widget-13">
                                                              <div className="k-widget-13__body">
                                                                  <a className="k-widget-13__title" href="index.html#">Reached 50,000 sales</a>
                                                                  <div className="k-widget-13__desc">
                                                                      To start a blog, think of a topic about and first brainstorm party is ways to write details
                                                                  </div>
                                                              </div>
                                                              <div className="k-widget-13__foot">
                                                                  <div className="k-widget-13__progress">
                                                                      <div className="k-widget-13__progress-info">
                                                                          <div className="k-widget-13__progress-status">
                                                                              Progress
                                                                          </div>
                                                                          <div className="k-widget-13__progress-value">24%</div>
                                                                      </div>
                                                                      <div className="progress">
                                                                          <div className="progress-bar k-bg-brand w24" role="progressbar" aria-valuenow="24" aria-valuemin="0" aria-valuemax="100"></div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                  </div>
                                  <div className="col-lg-6 col-xl-6 order-lg-2 order-xl-1">

                                      <div className="k-portlet k-portlet--height-fluid">
                                          <div className="k-portlet__head">
                                              <div className="k-portlet__head-label">
                                                  <h3 className="k-portlet__head-title">Latest Uploads</h3>
                                              </div>
                                              <div className="k-portlet__head-toolbar">
                                                  <div className="k-portlet__head-actions">
                                                      <a href="index.html#" className="btn btn-default btn-upper btn-sm btn-bold">All FILES</a>
                                                  </div>
                                              </div>
                                          </div>
                                          <div className="k-portlet__body k-portlet__body--fit k-portlet__body--fluid">
                                              <div className="k-widget-7">
                                                  <div className="k-widget-7__items">
                                                      <div className="k-widget-7__item">
                                                          <div className="k-widget-7__item-pic">
                                                              <img src={require( "../assets/media/files/doc.svg")} alt="" />
                                                          </div>
                                                          <div className="k-widget-7__item-info">
                                                              <a href="index.html#" className="k-widget-7__item-title">
                                  Keeg Design Reqs
                              </a>
                                                              <div className="k-widget-7__item-desc">
                                                                  95 MB
                                                              </div>
                                                          </div>
                                                          <div className="k-widget-7__item-toolbar">
                                                              <div className="dropdown dropdown-inline">
                                                                  <button type="button" className="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                      <i className="flaticon-more-1"></i>
                                                                  </button>
                                                                  <div className="dropdown-menu dropdown-menu-right">
                                                                      <ul className="k-nav">
                                                                          <li className="k-nav__section k-nav__section--first">
                                                                              <span className="k-nav__section-text">EXPORT TOOLS</span>
                                                                          </li>
                                                                          <li className="k-nav__item">
                                                                              <a href="index.html#" className="k-nav__link">
                                                                                  <i className="k-nav__link-icon la la-eye"></i>
                                                                                  <span className="k-nav__link-text">View</span>
                                                                              </a>
                                                                          </li>
                                                                          <li className="k-nav__item">
                                                                              <a href="index.html#" className="k-nav__link">
                                                                                  <i className="k-nav__link-icon la la-comment-o"></i>
                                                                                  <span className="k-nav__link-text">Coments</span>
                                                                              </a>
                                                                          </li>
                                                                          <li className="k-nav__item">
                                                                              <a href="index.html#" className="k-nav__link">
                                                                                  <i className="k-nav__link-icon la la-copy"></i>
                                                                                  <span className="k-nav__link-text">Copy</span>
                                                                              </a>
                                                                          </li>
                                                                          <li className="k-nav__item">
                                                                              <a href="index.html#" className="k-nav__link">
                                                                                  <i className="k-nav__link-icon la la-file-excel-o"></i>
                                                                                  <span className="k-nav__link-text">Excel</span>
                                                                              </a>
                                                                          </li>
                                                                      </ul>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div className="k-widget-7__foot">
                                                      <img src={require( "../assets/media/misc/clouds.png")} alt="" />
                                                      <div className="k-widget-7__upload">
                                                          <a href="index.html#"><i className="flaticon-upload"></i></a>
                                                          <span>Drag files here</span>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                  </div>

                                  <div className="col-lg-5 col-xl-5 order-lg-1 order-xl-1">
                                      <div className="k-portlet k-portlet--tabs k-portlet--height-fluid">
                                          <div className="k-portlet__head">
                                              <div className="k-portlet__head-label">
                                                  <h3 className="k-portlet__head-title">
                      Notifications                        
                  </h3>
                                              </div>
                                              <div className="k-portlet__head-toolbar">
                                                  <ul className="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-bold" role="tablist">
                                                      <li className="nav-item">
                                                          <a className="nav-link active" data-toggle="tab" href="index.html#k_portlet_tabs_1_1_1_content" role="tab">
                          Today
                          </a>
                                                      </li>
                                                      <li className="nav-item">
                                                          <a className="nav-link" data-toggle="tab" href="index.html#k_portlet_tabs_1_1_2_content" role="tab">
                          Week
                          </a>
                                                      </li>
                                                      <li className="nav-item">
                                                          <a className="nav-link" data-toggle="tab" href="index.html#k_portlet_tabs_1_1_3_content" role="tab">
                          Month
                          </a>
                                                      </li>
                                                  </ul>
                                              </div>
                                          </div>
                                          <div className="k-portlet__body">
                                              <div className="tab-content">
                                                  <div className="tab-pane fade active show" id="k_portlet_tabs_1_1_1_content" role="tabpanel">
                                                      <div className="k-scroll h420" data-scroll="true" data-mobile-height="350">

                                                          <div className="k-timeline">

                                                              <div className="k-timeline__item k-timeline__item--success">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-feed k-font-success"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">02:30 PM</span>
                                                                  </div>

                                                                  <a href="index.html" className="k-timeline__item-text">
                                      Jaana Abrogena added new Project.                                                                                             
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      Poster, App, Design
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--danger">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-safe-shield-protection k-font-danger"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">01:20 AM</span>
                                                                  </div>

                                                                  <a href="index.html" className="k-timeline__item-text">
                                      System updates                                                                                             
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      Info
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--brand">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon2-box k-font-brand"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">Yestardey</span>
                                                                  </div>

                                                                  <a href="index.html" className="k-timeline__item-text">
                                      FlyMore design mock-ups been uploadet by designers Bob, Naomi, Richard                                                                                            
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      PSD, Sketch, AJ
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--warning">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-pie-chart-1 k-font-warning"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">Aug 13,2018</span>
                                                                  </div>

                                                                  <a href="index.html" className="k-timeline__item-text">
                                      Meeting with Ken Digital Corp ot Unit14, 3 Edigor Buildings, George Street, Loondon<br/> England, BA12FJ                                                                                           
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      Meeting, Customer
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--info">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-notepad k-font-info"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">May 09, 2018</span>
                                                                  </div>

                                                                  <a href="index.html" className="k-timeline__item-text">
                                      KeenThemes created new layout whith tens of new options for Keen Admin panel                                                                                                
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      HTML,CSS,VueJS
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--accent">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-bell k-font-accent"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">01:20 AM</span>
                                                                  </div>

                                                                  <a href="index.html" className="k-timeline__item-text">
                                      New secyrity alert by Firewall & order to take aktion on User Preferences                                                                                             
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      Security, Fieewall
                                                                  </div>
                                                              </div>

                                                          </div>

                                                      </div>
                                                  </div>
                                                  <div className="tab-pane fade" id="k_portlet_tabs_1_1_2_content" role="tabpanel">
                                                      <div className="k-scroll h420" data-scroll="true" data-mobile-height="350">

                                                          <div className="k-timeline">

                                                              <div className="k-timeline__item k-timeline__item--info">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-psd  k-font-info"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">01:20 AM</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     New secyrity alert by Firewall &amp; order to take<br/> aktion on User Preferences                      
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      Security, Fieewall
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--success">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-pie-chart-1 k-font-success"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">02:30 PM</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     KeenThemes created new layout whith tens of<br/> new options for Keen Admin panel                   
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      HTML,CSS,VueJS
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--accent">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-shopping-basket k-font-accent"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">01:20 AM</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     New secyrity alert by Firewall &amp; order to take<br/> aktion on User references                   
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      Security, Fieewall
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--warning">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-rotate k-font-warning"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">May 09, 2018</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     KeenThemes created new layout whith tens of<br/> new options for Keen Admin panel                    
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      HTML,CSS,VueJS
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--brand">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-paper-plane-1 k-font-brand"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">Aug 13,2018</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     Meeting with Ken Digital Corp ot Unit14, 3<br/> Edigor Buildings, George Street, Loondon<br/> England, BA12FJ                      
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      Meeting, Customer
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--danger">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-pie-chart-1 k-font-danger"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">Yestardey</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     FlyMore design mock-ups been uploadet by<br/> designers Bob, Naomi, Richard                        
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      PSD, Sketch, AJ
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--warning">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-security k-font-warning"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">Yestardey</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     FlyMore design mock-ups been uploadet by<br/> designers Bob, Naomi, Richard                        
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      HTML,CSS,VueJS
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--brand">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-price-tag k-font-brand"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">02:30 PM</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     KeenThemes created new layout whith tens of<br/> new options for Keen Admin panel                       
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      HTML,CSS,VueJS
                                                                  </div>
                                                              </div>

                                                          </div>

                                                      </div>
                                                  </div>
                                                  <div className="tab-pane fade" id="k_portlet_tabs_1_1_3_content" role="tabpanel">
                                                      <div className="k-scroll h420" data-scroll="true" data-mobile-height="350">

                                                          <div className="k-timeline">

                                                              <div className="k-timeline__item k-timeline__item--brand">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-medal k-font-brand"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">Aug 13,2018</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     Meeting with Ken Digital Corp ot Unit14, 3<br/> Edigor Buildings, George Street, Loondon<br/> England, BA12FJ                       
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      Meeting, Customer
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--danger">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-safe-shield-protection k-font-danger"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">Yestardey</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     FlyMore design mock-ups been uploadet by<br/> designers Bob, Naomi, Richard                        
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      PSD, Sketch, AJ
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--info">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon2-box  k-font-info"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">01:20 AM</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     New secyrity alert by Firewall &amp; order to take<br/> aktion on User Preferences                          
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      Security, Fieewall
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--success">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-pie-chart-1 k-font-success"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">02:30 PM</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     KeenThemes created new layout whith tens of<br/> new options for Keen Admin panel                          
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      HTML,CSS,VueJS
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--accent">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-envelope k-font-accent"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">01:20 AM</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     New secyrity alert by Firewall &amp; order to take<br/> aktion on User references                           
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      Security, Fieewall
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--warning">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-rotate k-font-warning"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">May 09, 2018</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     KeenThemes created new layout whith tens of<br/> new options for Keen Admin panel                          
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      HTML,CSS,VueJS
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--info">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-feed k-font-info"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">Yestardey</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     FlyMore design mock-ups been uploadet by<br/> designers Bob, Naomi, Richard                            
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      HTML,CSS,VueJS
                                                                  </div>
                                                              </div>

                                                              <div className="k-timeline__item k-timeline__item--brand">
                                                                  <div className="k-timeline__item-section">
                                                                      <div className="k-timeline__item-section-border">
                                                                          <div className="k-timeline__item-section-icon">
                                                                              <i className="flaticon-download-1 k-font-brand"></i>
                                                                          </div>
                                                                      </div>
                                                                      <span className="k-timeline__item-datetime">02:30 PM</span>
                                                                  </div>
                                                                  <a href="index.html" className="k-timeline__item-text">                                
                                     KeenThemes created new layout whith tens of<br/> new options for Keen Admin panel                        
                                  </a>
                                                                  <div className="k-timeline__item-info">
                                                                      HTML,CSS,VueJS
                                                                  </div>
                                                              </div>

                                                          </div>

                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div className="col-lg-7 col-xl-7 order-lg-2 order-xl-1">

                                      <div className="k-portlet k-portlet--height-fluid k-widget ">
                                          <div className="k-portlet__body">
                                              <div id="k-widget-slider-13-1" className="k-slider carousel slide" data-ride="carousel" data-interval="8000">
                                                  <div className="k-slider__head">
                                                      <div className="k-slider__label">Calendar</div>
                                                      <div className="k-slider__nav">
                                                          <a className="k-slider__nav-prev carousel-control-prev" href="index.html#k-widget-slider-13-1" role="button" data-slide="prev">
                                                              <i className="fa fa-angle-left"></i>
                                                          </a>
                                                          <a className="k-slider__nav-next carousel-control-next" href="index.html#k-widget-slider-13-1" role="button" data-slide="next">
                                                              <i className="fa fa-angle-right"></i>
                                                          </a>
                                                      </div>
                                                  </div>
                                                  <table className="fc-list-table k-widget-12">
                                                      <tbody>
                                                          <tr className="fc-list-heading" data-date="2019-02-24">
                                                              <td className="fc-widget-header" colSpan="3">
                                                                  <a className="fc-list-heading-main" data-goto="{&quot;date&quot;:&quot;2019-02-24&quot;,&quot;type&quot;:&quot;day&quot;}">Sunday</a>
                                                                  <a className="fc-list-heading-alt" data-goto="{&quot;date&quot;:&quot;2019-02-24&quot;,&quot;type&quot;:&quot;day&quot;}">February 24, 2019</a>
                                                              </td>
                                                          </tr>
                                                          <tr className="fc-list-item fc-event-accent">
                                                              <td className="fc-list-item-time fc-widget-content">all-day</td>
                                                              <td className="fc-list-item-marker fc-widget-content">
                                                                  <span className="fc-event-dot"></span>
                                                              </td>
                                                              <td className="fc-list-item-title fc-widget-content"><a>Conference</a>
                                                                  <div className="fc-description">Lorem ipsum dolor eius mod tempor labore</div>
                                                              </td>
                                                              <td className="fc-list-item-time fc-widget-content">
                                                                  <div className="k-widget-12__members">
                                                                      <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="John Myer">
                                                                          <img src={require( "../assets/media/users/100_1.jpg")} alt="image" />
                                                                      </a>
                                                                      <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="Alison Brandy">
                                                                          <img src={require( "../assets/media/users/100_10.jpg")} alt="image" />
                                                                      </a>
                                                                      <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="Selina Cranson">
                                                                          <img src={require( "../assets/media/users/100_1.jpg")} alt="image" />
                                                                      </a>
                                                                      <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="Luke Walls">
                                                                          <img src={require( "../assets/media/users/100_1.jpg")} alt="image" />
                                                                      </a>
                                                                      <a href="general.html#" className="k-widget-12__member k-widget-12__member--last">
                                                      +10
                                                  </a>
                                                                  </div>
                                                              </td>
                                                          </tr>
                                                          <tr className="fc-list-item">
                                                              <td className="fc-list-item-time fc-widget-content">10:30am - 12:30pm</td>
                                                              <td className="fc-list-item-marker fc-widget-content">
                                                                  <span className="fc-event-dot"></span>
                                                              </td>
                                                              <td className="fc-list-item-title fc-widget-content">
                                                                  <a>Meeting</a>
                                                                  <div className="fc-description">Lorem ipsum dolor eiu idunt ut labore
                                                                  </div>
                                                              </td>
                                                              <td className="fc-list-item-time fc-widget-content">
                                                                  <div className="k-widget-12__members">
                                                                      <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="John Myer">
                                                                          <img src={require( "../assets/media/users/100_1.jpg")} alt="image" />
                                                                      </a>
                                                                      <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="Alison Brandy">
                                                                          <img src={require( "../assets/media/users/100_10.jpg")} alt="image" />
                                                                      </a>
                                                                      <a href="general.html#" className="k-widget-12__member k-widget-12__member--last">
                                                      +10
                                                  </a>
                                                                  </div>
                                                              </td>
                                                          </tr>
                                                          <tr className="fc-list-item fc-event-info">
                                                              <td className="fc-list-item-time fc-widget-content">12:00pm</td>
                                                              <td className="fc-list-item-marker fc-widget-content">
                                                                  <span className="fc-event-dot"></span>
                                                              </td>
                                                              <td className="fc-list-item-title fc-widget-content">
                                                                  <a>Lunch</a>
                                                                  <div className="fc-description">Lorem ipsum dolor sit amet, ut labore</div>
                                                              </td>
                                                              <td className="fc-list-item-time fc-widget-content">
                                                                  <div className="k-widget-12__members">
                                                                      <a href="general.html#" className="k-widget-12__member" data-toggle="k-tooltip" data-skin="brand" data-placement="top" title="John Myer">
                                                                          <img src={require( "../assets/media/users/100_1.jpg")} alt="image" />
                                                                      </a>
                                                                  </div>
                                                              </td>
                                                          </tr>
                                                          <tr className="fc-list-item fc-event-warning">
                                                              <td className="fc-list-item-time fc-widget-content">2:30pm</td>
                                                              <td className="fc-list-item-marker fc-widget-content">
                                                                  <span className="fc-event-dot"></span>
                                                              </td>
                                                              <td className="fc-list-item-title fc-widget-content">
                                                                  <a>Meeting</a>
                                                                  <div className="fc-description">Lorem ipsum conse ctetur adipi scing
                                                                  </div>
                                                              </td>
                                                          </tr>
                                                      </tbody>
                                                  </table>
                                              </div>
                                          </div>
                                      </div>

                                  </div>

                              </div>

                          </div>
                      </div>

                      <div className="k-footer k-grid__item k-grid k-grid--desktop k-grid--ver-desktop">
                          <div className="k-footer__copyright">
                              2019&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/keen" target="_blank" className="k-link">Dice205</a>
                          </div>
                          <div className="k-footer__menu">
                              <a href="http://keenthemes.com/keen" target="_blank" className="k-footer__menu-link k-link">About</a>
                              <a href="http://keenthemes.com/keen" target="_blank" className="k-footer__menu-link k-link">Team</a>
                              <a href="http://keenthemes.com/keen" target="_blank" className="k-footer__menu-link k-link">Contact</a>
                          </div>
                      </div>
                  </div>

              </div>
          </div>

          <div id="k_offcanvas_toolbar_quick_actions" className="k-offcanvas-panel">
              <div className="k-offcanvas-panel__head">
                  <h3 className="k-offcanvas-panel__title">
            Quick Actions
          </h3>
                  <a href="index.html#" className="k-offcanvas-panel__close" id="k_offcanvas_toolbar_quick_actions_close"><i className="flaticon2-delete"></i></a>
              </div>
              <div className="k-offcanvas-panel__body">
                  <div className="k-grid-nav-v2">
                      <a href="index.html#" className="k-grid-nav-v2__item">
                          <div className="k-grid-nav-v2__item-icon"><i className="flaticon2-box"></i></div>
                          <div className="k-grid-nav-v2__item-title">Orders</div>
                      </a>
                      <a href="index.html#" className="k-grid-nav-v2__item">
                          <div className="k-grid-nav-v2__item-icon"><i className="flaticon-download-1"></i></div>
                          <div className="k-grid-nav-v2__item-title">Uploades</div>
                      </a>
                      <a href="index.html#" className="k-grid-nav-v2__item">
                          <div className="k-grid-nav-v2__item-icon"><i className="flaticon2-supermarket"></i></div>
                          <div className="k-grid-nav-v2__item-title">Products</div>
                      </a>
                      <a href="index.html#" className="k-grid-nav-v2__item">
                          <div className="k-grid-nav-v2__item-icon"><i className="flaticon2-avatar"></i></div>
                          <div className="k-grid-nav-v2__item-title">Customers</div>
                      </a>
                      <a href="index.html#" className="k-grid-nav-v2__item">
                          <div className="k-grid-nav-v2__item-icon"><i className="flaticon2-list"></i></div>
                          <div className="k-grid-nav-v2__item-title">Blog Posts</div>
                      </a>
                      <a href="index.html#" className="k-grid-nav-v2__item">
                          <div className="k-grid-nav-v2__item-icon"><i className="flaticon2-settings"></i></div>
                          <div className="k-grid-nav-v2__item-title">Settings</div>
                      </a>
                  </div>
              </div>
          </div>

          <div id="k_quick_panel" className="k-offcanvas-panel">
              <div className="k-offcanvas-panel__nav">
                  <ul className="nav nav-pills" role="tablist">
                      <li className="nav-item active">
                          <a className="nav-link active" data-toggle="tab" href="index.html#k_quick_panel_tab_notifications" role="tab">Notifications</a>
                      </li>
                      <li className="nav-item">
                          <a className="nav-link" data-toggle="tab" href="index.html#k_quick_panel_tab_actions" role="tab">Actions</a>
                      </li>
                      <li className="nav-item">
                          <a className="nav-link" data-toggle="tab" href="index.html#k_quick_panel_tab_settings" role="tab">Settings</a>
                      </li>
                  </ul>

                  <button className="k-offcanvas-panel__close" id="k_quick_panel_close_btn"><i className="flaticon2-delete"></i></button>
              </div>

              <div className="k-offcanvas-panel__body">
                  <div className="tab-content">
                      <div className="tab-pane fade show k-offcanvas-panel__content k-scroll active" id="k_quick_panel_tab_notifications" role="tabpanel">

                          <div className="k-timeline">

                              <div className="k-timeline__item k-timeline__item--success">
                                  <div className="k-timeline__item-section">
                                      <div className="k-timeline__item-section-border">
                                          <div className="k-timeline__item-section-icon">
                                              <i className="flaticon-feed k-font-success"></i>
                                          </div>
                                      </div>
                                      <span className="k-timeline__item-datetime">02:30 PM</span>
                                  </div>

                                  <a href="index.html" className="k-timeline__item-text">
                                  KeenThemes created new layout whith tens of new options for Keen Admin panel                                                                                             
                              </a>
                                  <div className="k-timeline__item-info">
                                      HTML,CSS,VueJS
                                  </div>
                              </div>

                              <div className="k-timeline__item k-timeline__item--danger">
                                  <div className="k-timeline__item-section">
                                      <div className="k-timeline__item-section-border">
                                          <div className="k-timeline__item-section-icon">
                                              <i className="flaticon-safe-shield-protection k-font-danger"></i>
                                          </div>
                                      </div>
                                      <span className="k-timeline__item-datetime">01:20 AM</span>
                                  </div>

                                  <a href="index.html" className="k-timeline__item-text">
                                  New secyrity alert by Firewall & order to take aktion on User Preferences                                                                                             
                              </a>
                                  <div className="k-timeline__item-info">
                                      Security, Fieewall
                                  </div>
                              </div>

                              <div className="k-timeline__item k-timeline__item--brand">
                                  <div className="k-timeline__item-section">
                                      <div className="k-timeline__item-section-border">
                                          <div className="k-timeline__item-section-icon">
                                              <i className="flaticon2-box k-font-brand"></i>
                                          </div>
                                      </div>
                                      <span className="k-timeline__item-datetime">Yestardey</span>
                                  </div>

                                  <a href="index.html" className="k-timeline__item-text">
                                  FlyMore design mock-ups been uploadet by designers Bob, Naomi, Richard                                                                                            
                              </a>
                                  <div className="k-timeline__item-info">
                                      PSD, Sketch, AJ
                                  </div>
                              </div>

                              <div className="k-timeline__item k-timeline__item--warning">
                                  <div className="k-timeline__item-section">
                                      <div className="k-timeline__item-section-border">
                                          <div className="k-timeline__item-section-icon">
                                              <i className="flaticon-pie-chart-1 k-font-warning"></i>
                                          </div>
                                      </div>
                                      <span className="k-timeline__item-datetime">Aug 13,2018</span>
                                  </div>

                                  <a href="index.html" className="k-timeline__item-text">
                                  Meeting with Ken Digital Corp ot Unit14, 3 Edigor Buildings, George Street, Loondon<br/> England, BA12FJ                                                                                           
                              </a>
                                  <div className="k-timeline__item-info">
                                      Meeting, Customer
                                  </div>
                              </div>

                              <div className="k-timeline__item k-timeline__item--info">
                                  <div className="k-timeline__item-section">
                                      <div className="k-timeline__item-section-border">
                                          <div className="k-timeline__item-section-icon">
                                              <i className="flaticon-notepad k-font-info"></i>
                                          </div>
                                      </div>
                                      <span className="k-timeline__item-datetime">May 09, 2018</span>
                                  </div>

                                  <a href="index.html" className="k-timeline__item-text">
                                  KeenThemes created new layout whith tens of new options for Keen Admin panel                                                                                                
                              </a>
                                  <div className="k-timeline__item-info">
                                      HTML,CSS,VueJS
                                  </div>
                              </div>

                              <div className="k-timeline__item k-timeline__item--accent">
                                  <div className="k-timeline__item-section">
                                      <div className="k-timeline__item-section-border">
                                          <div className="k-timeline__item-section-icon">
                                              <i className="flaticon-bell k-font-accent"></i>
                                          </div>
                                      </div>
                                      <span className="k-timeline__item-datetime">01:20 AM</span>
                                  </div>

                                  <a href="index.html" className="k-timeline__item-text">
                                  New secyrity alert by Firewall & order to take aktion on User Preferences                                                                                             
                              </a>
                                  <div className="k-timeline__item-info">
                                      Security, Fieewall
                                  </div>
                              </div>

                          </div>

                      </div>
                      <div className="tab-pane fade k-offcanvas-panel__content k-scroll" id="k_quick_panel_tab_actions" role="tabpanel">

                          <div className="k-portlet k-portlet--solid-success">
                              <div className="k-portlet__head">
                                  <div className="k-portlet__head-label">
                                      <span className="k-portlet__head-icon k-hide"><i className="flaticon-stopwatch"></i></span>
                                      <h3 className="k-portlet__head-title">Recent Bills</h3>
                                  </div>
                                  <div className="k-portlet__head-toolbar">
                                      <div className="k-portlet__head-group">
                                          <div className="dropdown dropdown-inline">
                                              <button type="button" className="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                  <i className="flaticon-more"></i>
                                              </button>
                                              <div className="dropdown-menu dropdown-menu-right">
                                                  <a className="dropdown-item" href="index.html#">Action</a>
                                                  <a className="dropdown-item" href="index.html#">Another action</a>
                                                  <a className="dropdown-item" href="index.html#">Something else here</a>
                                                  <div className="dropdown-divider"></div>
                                                  <a className="dropdown-item" href="index.html#">Separated link</a>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div className="k-portlet__body">
                                  <div className="k-portlet__content">
                                      Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry.
                                  </div>
                              </div>
                              <div className="k-portlet__foot k-portlet__foot--sm k-align-right">
                                  <a href="index.html#" className="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                                  <a href="index.html#" className="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                              </div>
                          </div>

                          <div className="k-portlet k-portlet--solid-focus">
                              <div className="k-portlet__head">
                                  <div className="k-portlet__head-label">
                                      <span className="k-portlet__head-icon k-hide"><i className="flaticon-stopwatch"></i></span>
                                      <h3 className="k-portlet__head-title">Latest Orders</h3>
                                  </div>
                                  <div className="k-portlet__head-toolbar">
                                      <div className="k-portlet__head-group">
                                          <div className="dropdown dropdown-inline">
                                              <button type="button" className="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                  <i className="flaticon-more"></i>
                                              </button>
                                              <div className="dropdown-menu dropdown-menu-right">
                                                  <a className="dropdown-item" href="index.html#">Action</a>
                                                  <a className="dropdown-item" href="index.html#">Another action</a>
                                                  <a className="dropdown-item" href="index.html#">Something else here</a>
                                                  <div className="dropdown-divider"></div>
                                                  <a className="dropdown-item" href="index.html#">Separated link</a>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div className="k-portlet__body">
                                  <div className="k-portlet__content">
                                      Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry.
                                  </div>
                              </div>
                              <div className="k-portlet__foot k-portlet__foot--sm k-align-right">
                                  <a href="index.html#" className="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                                  <a href="index.html#" className="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                              </div>
                          </div>

                          <div className="k-portlet k-portlet--solid-info">
                              <div className="k-portlet__head">
                                  <div className="k-portlet__head-label">
                                      <h3 className="k-portlet__head-title">Latest Invoices</h3>
                                  </div>
                                  <div className="k-portlet__head-toolbar">
                                      <div className="k-portlet__head-group">
                                          <div className="dropdown dropdown-inline">
                                              <button type="button" className="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                  <i className="flaticon-more"></i>
                                              </button>
                                              <div className="dropdown-menu dropdown-menu-right">
                                                  <a className="dropdown-item" href="index.html#">Action</a>
                                                  <a className="dropdown-item" href="index.html#">Another action</a>
                                                  <a className="dropdown-item" href="index.html#">Something else here</a>
                                                  <div className="dropdown-divider"></div>
                                                  <a className="dropdown-item" href="index.html#">Separated link</a>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div className="k-portlet__body">
                                  <div className="k-portlet__content">
                                      Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry.
                                  </div>
                              </div>
                              <div className="k-portlet__foot k-portlet__foot--sm k-align-right">
                                  <a href="index.html#" className="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                                  <a href="index.html#" className="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                              </div>
                          </div>

                          <div className="k-portlet k-portlet--solid-warning">
                              <div className="k-portlet__head">
                                  <div className="k-portlet__head-label">
                                      <h3 className="k-portlet__head-title">New Comments</h3>
                                  </div>
                                  <div className="k-portlet__head-toolbar">
                                      <div className="k-portlet__head-group">
                                          <div className="dropdown dropdown-inline">
                                              <button type="button" className="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                  <i className="flaticon-more"></i>
                                              </button>
                                              <div className="dropdown-menu dropdown-menu-right">
                                                  <a className="dropdown-item" href="index.html#">Action</a>
                                                  <a className="dropdown-item" href="index.html#">Another action</a>
                                                  <a className="dropdown-item" href="index.html#">Something else here</a>
                                                  <div className="dropdown-divider"></div>
                                                  <a className="dropdown-item" href="index.html#">Separated link</a>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div className="k-portlet__body">
                                  <div className="k-portlet__content">
                                      Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry.
                                  </div>
                              </div>
                              <div className="k-portlet__foot k-portlet__foot--sm k-align-right">
                                  <a href="index.html#" className="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                                  <a href="index.html#" className="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                              </div>
                          </div>

                          <div className="k-portlet k-portlet--solid-brand">
                              <div className="k-portlet__head">
                                  <div className="k-portlet__head-label">
                                      <h3 className="k-portlet__head-title">Recent Posts</h3>
                                  </div>
                                  <div className="k-portlet__head-toolbar">
                                      <div className="k-portlet__head-group">
                                          <div className="dropdown dropdown-inline">
                                              <button type="button" className="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                  <i className="flaticon-more"></i>
                                              </button>
                                              <div className="dropdown-menu dropdown-menu-right">
                                                  <a className="dropdown-item" href="index.html#">Action</a>
                                                  <a className="dropdown-item" href="index.html#">Another action</a>
                                                  <a className="dropdown-item" href="index.html#">Something else here</a>
                                                  <div className="dropdown-divider"></div>
                                                  <a className="dropdown-item" href="index.html#">Separated link</a>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div className="k-portlet__body">
                                  <div className="k-portlet__content">
                                      Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry.
                                  </div>
                              </div>
                              <div className="k-portlet__foot k-portlet__foot--sm k-align-right">
                                  <a href="index.html#" className="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                                  <a href="index.html#" className="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                              </div>
                          </div>

                      </div>
                      <div className="tab-pane fade k-offcanvas-panel__content k-scroll" id="k_quick_panel_tab_settings" role="tabpanel">
                          <form className="k-form">
                              <div className="k-heading k-heading--space-sm">Notifications</div>

                              <div className="form-group form-group-xs row">
                                  <label className="col-8 col-form-label">Enable notifications:</label>
                                  <div className="col-4 k-align-right">
                                      <span className="k-switch k-switch--sm">
                      <label>
                        <input type="checkbox" checked="checked" name="quick_panel_notifications_1"/>
                        <span></span>
                                      </label>
                                      </span>
                                  </div>
                              </div>
                              <div className="form-group form-group-xs row">
                                  <label className="col-8 col-form-label">Enable audit log:</label>
                                  <div className="col-4 k-align-right">
                                      <span className="k-switch k-switch--sm">
                      <label>
                        <input type="checkbox"  name="quick_panel_notifications_2"/>
                        <span></span>
                                      </label>
                                      </span>
                                  </div>
                              </div>
                              <div className="form-group form-group-last form-group-xs row">
                                  <label className="col-8 col-form-label">Notify on new orders:</label>
                                  <div className="col-4 k-align-right">
                                      <span className="k-switch k-switch--sm">
                      <label>
                        <input type="checkbox" checked="checked" name="quick_panel_notifications_2"/>
                        <span></span>
                                      </label>
                                      </span>
                                  </div>
                              </div>

                              <div className="k-separator k-separator--space-md k-separator--border-dashed"></div>

                              <div className="k-heading k-heading--space-sm">Orders</div>

                              <div className="form-group form-group-xs row">
                                  <label className="col-8 col-form-label">Enable order tracking:</label>
                                  <div className="col-4 k-align-right">
                                      <span className="k-switch k-switch--sm k-switch--danger">
                      <label>
                        <input type="checkbox" checked="checked" name="quick_panel_notifications_3"/>
                        <span></span>
                                      </label>
                                      </span>
                                  </div>
                              </div>
                              <div className="form-group form-group-xs row">
                                  <label className="col-8 col-form-label">Enable orders reports:</label>
                                  <div className="col-4 k-align-right">
                                      <span className="k-switch k-switch--sm k-switch--danger">
                      <label>
                        <input type="checkbox"  name="quick_panel_notifications_3"/>
                        <span></span>
                                      </label>
                                      </span>
                                  </div>
                              </div>
                              <div className="form-group form-group-last form-group-xs row">
                                  <label className="col-8 col-form-label">Allow order status auto update:</label>
                                  <div className="col-4 k-align-right">
                                      <span className="k-switch k-switch--sm k-switch--danger">
                      <label>
                        <input type="checkbox" checked="checked" name="quick_panel_notifications_4"/>
                        <span></span>
                                      </label>
                                      </span>
                                  </div>
                              </div>

                              <div className="k-separator k-separator--space-md k-separator--border-dashed"></div>

                              <div className="k-heading k-heading--space-sm">Customers</div>

                              <div className="form-group form-group-xs row">
                                  <label className="col-8 col-form-label">Enable customer singup:</label>
                                  <div className="col-4 k-align-right">
                                      <span className="k-switch k-switch--sm k-switch--success">
                      <label>
                        <input type="checkbox" checked="checked" name="quick_panel_notifications_5"/>
                        <span></span>
                                      </label>
                                      </span>
                                  </div>
                              </div>
                              <div className="form-group form-group-xs row">
                                  <label className="col-8 col-form-label">Enable customers reporting:</label>
                                  <div className="col-4 k-align-right">
                                      <span className="k-switch k-switch--sm k-switch--success">
                      <label>
                        <input type="checkbox"  name="quick_panel_notifications_5"/>
                        <span></span>
                                      </label>
                                      </span>
                                  </div>
                              </div>
                              <div className="form-group form-group-last form-group-xs row">
                                  <label className="col-8 col-form-label">Notifiy on new customer registration:</label>
                                  <div className="col-4 k-align-right">
                                      <span className="k-switch k-switch--sm k-switch--success">
                      <label>
                        <input type="checkbox" checked="checked" name="quick_panel_notifications_6"/>
                        <span></span>
                                      </label>
                                      </span>
                                  </div>
                              </div>

                          </form>
                      </div>
                  </div>
              </div>
          </div>

          <div id="k_scrolltop" className="k-scrolltop">
              <i className="la la-arrow-up"></i>
          </div>

      </div>   
    );
  }
    
}
}

export default withRouter(Home);