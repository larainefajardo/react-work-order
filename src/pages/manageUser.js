import React, { Component } from 'react';
import '../App.css';
import { writeData, clearAllData, prettifyDate, formatDateByNumber } from '../commons/utility';
import { Modal, Loader, UserForm } from '../commons/utilityViews.js';
import Sidebar from '../components/sidebar';
import '../home.css';
import DatePicker from 'react-date-picker';
import {Doughnut} from 'react-chartjs-2';
import { myConfig } from '../commons/config';
import { apiModule } from '../commons/apiCall.js';
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import { withRouter } from 'react-router-dom';
import 'react-vertical-timeline-component/style.min.css';
class ManageUser extends Component {

  constructor(props) {
    super(props);
    this.state = {
      toggle : undefined,
      userList : [],
      userDetails : {},
      show : false,
      isLoading : true,
      search: '',
      showDelete: false,
      showRegister: false,
      createUserDetails : { first_name : "" , last_name : "" , email : "", password : "", gender: false, admin : false, report : false, subscriber : false, contact_number : "", gender : "", country : "", city : "" }
    };
    this.handleUpdateUsers = this.handleUpdateUsers.bind(this);
    this.handleSearchRequest = this.handleSearchRequest.bind(this);
    this.handleSearchChanges = this.handleSearchChanges.bind(this);
    this.handleUserDetailsChanges = this.handleUserDetailsChanges.bind(this);
    this.handleCreateUserDetailsChanges = this.handleCreateUserDetailsChanges.bind(this);
    this.handleCreateUserDetailsRadioButtonChanges = this.handleCreateUserDetailsRadioButtonChanges.bind(this);
  }
  
 async componentDidMount(){
  let jwt = this.props.getSession().jwt;
  this.setState({jwt : jwt});
  // alert(jwt)
  await this.fetchUsers(this.props.getSession().jwt);
}

fetchUsers = async (jwt) => {
  let headerParams = { x_auth : jwt };
  let result = await apiModule('admin_get_all_users', headerParams, null);
  result = result.data.sort(function (a, b) {
  return a.last_name - b.last_name;
});
  this.setState({userList : result, isLoading : false});
}

handleUpdateUsers = async (event) => {
  event.preventDefault();
  let userDetails = this.state.userDetails;
  let postData = { first_name : userDetails.first_name, last_name : userDetails.last_name, email : userDetails.email, 
    country : userDetails.country, city : userDetails.city, gender : userDetails.gender, contact_number : userDetails.contact_number };
  let headerParams = { x_auth : this.state.jwt, user_id : userDetails.user_id };
  let result = await apiModule('admin_update_user', headerParams, postData);
  window.confirm(JSON.stringify(result))
}

handleCreateUserValidation(userDetails){
  if(userDetails.first_name == "" || userDetails.last_name == "" || userDetails.email == "" || userDetails.password == "" || userDetails.contry == "" ||
    userDetails.city == "" || userDetails.gender == "" || userDetails.contact_number == "" || userDetails.contact_number == "" && !userDetails.admin &&
    !userDetails.subscriber && !userDetails.report ){
    return false;
  }else{ 
    return true;
  }
}

handleCreateUser = async (event) => {
  event.preventDefault();
  let userDetails = this.state.createUserDetails;
  if(this.handleCreateUserValidation(userDetails)){
    let gender = userDetails.gender == true ? "F" : 'M';
    let role;
    if(userDetails["admin"]){
      role = myConfig.role[0];
    }else if(userDetails["subscriber"]){
      role = myConfig.role[1];
    }else{
      role = myConfig.role[2];
    }
    let postData = { first_name : userDetails.first_name, last_name : userDetails.last_name, email : userDetails.email, password :  userDetails.password,
      country : userDetails.country, city : userDetails.city, gender : gender, contact_number : userDetails.contact_number, role : JSON.stringify(role) };
    let headerParams = { x_auth : this.state.jwt,  user_id : userDetails.user_id };
    let result = await apiModule('admin_create_user', headerParams, postData);
    window.confirm(result.message);
    this.setState({showRegister : false , isLoading : true});
    await this.fetchUsers(this.props.getSession().jwt);
  }else{
    window.confirm("Please fill required fields");
  }
}

handleCreateUserDetailsRadioButtonChanges = (e) => {
  let userDetails = this.state.createUserDetails;
  userDetails[e.target.name] = !userDetails[e.target.name];
  if(e.target.name == "admin"){
    userDetails["subscriber"] = false;
    userDetails["report"] = false;
  }else if(e.target.name == "subscriber"){
    userDetails["admin"] = false;
    userDetails["report"] = false;
  }else if(e.target.name == "report"){
    userDetails["admin"] = false;
    userDetails["subscriber"] = false;
  }
  this.setState({userDetails});

}

handleUserDetailsChanges(e){
  let userDetails = this.state.userDetails;
  userDetails[e.target.name] = e.target.value;
  this.setState({userDetails});
}

handleCreateUserDetailsChanges(e){
  let userDetails = this.state.createUserDetails;
  userDetails[e.target.name] = e.target.value;
  this.setState({userDetails});
}

handleSearchChanges(e){
  this.setState({[e.target.name] : e.target.value})
}

handleDeleteUserRequest = async (event) => {
  let userDetails = this.state.userDetails;
  let userList = this.state.userList;
  let headerParams = { x_auth : this.state.jwt, user_id : userDetails.user_id };
  let data = await apiModule('admin_delete_user', headerParams, null);
  if(data.result == "success"){
    window.confirm(data.message);
    userList.splice(userList.findIndex(v => v.email === userDetails.email), 1);
    this.setState({userList, userDetails : {}, show : false})
  }
}


async handleSearchRequest(e){
  e.preventDefault();
  this.setState({isLoading : true});
  let postData = { qname : this.state.search };
  let headerParams = { x_auth : this.state.jwt };
  let result = await apiModule('admin_search_user', headerParams, postData);
  this.setState({userList : result, isLoading : false})
}

displayUserList = () => {
  return this.state.userList.map((item, i) => {
    return (
        <div key={i} className="cards card-1 ">
          <img className="img-wrap" src={require('../assets/icons/person-silhouette.png')}/>
          <div className="info-wrap">
            <label>{item.full_name}</label>
            <label>{item.email}</label>
          </div>
          <div className="col view-button" onClick={() => this.setState({userDetails : item, show : !this.state.show})} >View Details</div>
          <div className="col delete-button" onClick={(event) => {this.setState({userDetails : item, showDelete : true});}} >Delete User</div>
        </div>
      )
  });
}

deleteWarning(){
  const showHideClassName = this.state.showDelete ? 'popup display-block' : 'popup display-none';
  return (
    <div className={showHideClassName}>
      <div className='popup-foreground'>
        <div onClick={() => this.setState({showDelete : !this.state.showDelete})} className="icon-cards-batch">
          <i  className="fa fa-close icon-wrapper"></i>
        </div>
        Are you sure you want to delete this user?
        <div className="row warning-buttons">
        <label className="btn-warning-alert" onClick={() => this.handleDeleteUserRequest()} >YES</label> 
        <label className="btn-warning-alert" onClick={() => this.setState({showDelete : !this.state.showDelete})}>CANCEL</label>
        </div>
      </div>
    </div>
  )
}

showDetailsModal = () => {
  let userDetails = this.state.userDetails;
  const showHideClassName = this.state.show ? 'popup display-block' : 'popup display-none';
  return (
    <div className={showHideClassName}>
      <div className='popup-foreground'>
        <div onClick={() => this.setState({show : !this.state.show})} className="icon-cards-batch">
          <i  className="fa fa-close icon-wrapper"></i>
        </div>
        <div className="align-label">
          <div className="col full-field-label">{userDetails.full_name}</div>
        </div>
        <form className="form-user-details" onSubmit={this.handleUpdateUsers}>
          <div className="row pad3">
            <div className="col form-title-wrap">First Name:</div>
            <div className="col">
              <input type="text" className="user-fields" name="first_name" onChange={(event) => this.handleUserDetailsChanges(event) } value={userDetails.first_name}/>
            </div>
          </div>
          <div className="row pad3">
            <div className="col form-title-wrap">Last Name:</div>
            <div className="col">
              <input type="text" className="user-fields" name="last_name" onChange={(event) => this.handleUserDetailsChanges(event) } value={userDetails.last_name}/>
            </div>
          </div>
          <div className="row pad3">
            <div className="col form-title-wrap">Email:</div>
             <div className="col">
              <input type="text" className="user-fields"  name="email" onChange={(event) => this.handleUserDetailsChanges(event) } value={userDetails.email}/>
            </div>
          </div>
          <div className="row pad3">
          <div className="col form-title-wrap">Contact Number:</div>
          <div className="col">
              <input type="text" className="user-fields"  name="contact_number" onChange={(event) => this.handleUserDetailsChanges(event) } value={userDetails.contact_number}/>
            </div>
          </div>
          <div className="row pad3">
            <div className="col form-title-wrap">Gender:</div>
            <div className="col">
              <input type="text" className="user-fields"  name="gender" onChange={(event) => this.handleUserDetailsChanges(event) } value={userDetails.gender}/>
            </div>
          </div>
          <div className="row pad3">
            <div className="col form-title-wrap">Country:</div>
            <div className="col">
              <input type="text" className="user-fields"  name="country" onChange={(event) => this.handleUserDetailsChanges(event) } value={userDetails.country}/>
            </div>
          </div>
          <div className="row pad3">
            <div className="col form-title-wrap">City:</div>
            <div className="col">
              <input type="text" className="user-fields"  name="city" onChange={(event) => this.handleUserDetailsChanges(event) } value={userDetails.city}/>
            </div>
          </div>
          <div className="button-wrap">
          <button type="submit" class="btn btn-primary btn-md btn-margin">Save Changes</button> 
          </div>
        </form>
      </div>
    </div>
  );
}

toggleCreateUser(){
  this.setState({createUserDetails : {first_name : "" , last_name : "" , email : "", contact_number : "", gender : "", country : "", city : ""}}, () => {
    this.setState({showRegister : true});
    })
}

  render() {
    return (
      <div className="home">
        <div  className="box">
          <div className="box-wrap" onClick={() => myConfig.sidebarContext.toggleDrawer()}>
            <img className="menu-bar-breed" src={require('../assets/icons/menu.svg')}/>
            <span className="menu-label-breed">Manage User</span>
          </div>
        </div>
        <div id="floating-button" onClick={() => this.toggleCreateUser()}>
          <p className="plus">+</p>
        </div>
        <div className="align-search">
          <form onSubmit={this.handleSearchRequest} className="col-md-4">
            <input type="text" id="search-bar" name="search" onChange={(event) => this.handleSearchChanges(event)} value={this.state.search} placeholder="Who are you looking for?"/>
            <i className="fa fa-search search-icons" onClick={(event) => this.handleSearchRequest(event)}></i>
          </form>
        </div>
        <div className="row flex-wrap">
          {this.displayUserList()}
          <Loader show={this.state.isLoading}/>
          <UserForm show={this.state.showRegister} userDetails={this.state.createUserDetails} handleRadioButtonChanges={(event) => this.handleCreateUserDetailsRadioButtonChanges(event)} handleChanges={(event) => this.handleCreateUserDetailsChanges(event)}
          handleClose={() => this.setState({showRegister : !this.state.showRegister})} handleSubmit={(event) => this.handleCreateUser(event)} />
        </div>
          {this.showDetailsModal()}
          {this.deleteWarning()}
      </div>
    ); 
  }
}

export default withRouter(ManageUser);