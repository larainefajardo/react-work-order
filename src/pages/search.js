import React, { Component } from 'react';
import '../App.css';
import { writeData, clearAllData, prettifyDate, formatDateByNumber } from '../commons/utility';
import { Modal, Loader } from '../commons/utilityViews.js';
import Sidebar from '../components/sidebar';
import '../home.css';
import DatePicker from 'react-date-picker';
import {Doughnut} from 'react-chartjs-2';
import { myConfig } from '../commons/config';
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import { withRouter } from 'react-router-dom';
import 'react-vertical-timeline-component/style.min.css';
class Search extends Component {

  constructor(props) {
    super(props);
    this.state = {
      toggle : undefined,
    
    };
  }
  
async componentDidMount(){
  let jwt = this.props.getSession().jwt;
  this.setState({jwt : jwt});
  
}

  render() {
    return (
      <div className="home">
            <div  className="box">
              <div className="box-wrap" onClick={() => myConfig.sidebarContext.toggleDrawer()}>
                <img className="menu-bar-breed" src={require('../assets/icons/menu.svg')}/>
                <span className="menu-label-breed">Search</span>
              </div>
            </div>
            <div className="home-container">
              
            </div>
            <div className="row charts-container">
              <form className="search-container">
                <input type="text" id="search-bar" placeholder="What can I help you with today?"/>
                <i  className="fa fa-search search-icons"></i>
              </form>
            </div>
             
          </div>
        
    ); 
  }
}

export default withRouter(Search);