import React, { Component } from 'react';
import '../App.css';
import '../home.css';
import Select from 'react-select';
import { writeData, readAllData, clearAllData, jsonToURI } from '../commons/utility.js';
import { withRouter } from 'react-router-dom';

import { myConfig } from '../commons/config';
import { apiModule, offlineSync } from '../commons/apiCall.js';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' }
];
 

class Notifications extends Component {

  constructor(props) {
    super(props);
    this.state = {
      toggle : undefined,
      selectedOption: null,
      syncItem : {
        title: '',
        message: '',
        ids: ''
      }
    };
  }
  
 
  handleChange = (selectedOption) => {
    
    let syncItem = this.state.syncItem;
    let user = [];
    for(let i = 0; i < selectedOption.length; i++ ) {
      user.push(selectedOption[i].value + "");
    }
    this.state.syncItem.ids = user.join(",")
    this.setState({ selectedOption,  syncItem});
    
    console.log(`Option selected:`, selectedOption);
  }
  handleChanges = (event) => {
    console.log(event.target.name);
    let syncItem = this.state.syncItem;
    syncItem[event.target.id] = event.target.value;
    
    this.setState({syncItem: syncItem});
  }
  
  async componentDidMount(){
    let jwt = this.props.getSession().jwt;
    this.retrieveUsers();
    this.setState({jwt : jwt});
    
  }

  
  retrieveUsers() {
    let request = {x_auth : this.props.getSession().jwt}
    let that = this;
    
    apiModule('user_list_all', request, null).then(function(data) {
      console.log(data.data);
      let user = [];
      for(let i = 0; i < data.data.length; i++ ) {
        user.push({ label: data.data[i].first_name + " " + data.data[i].last_name , value: data.data[i].user_id });
      }
      that.setState({user : user});
    });
    
  }

  sendNotification() {
    let request = {x_auth : this.props.getSession().jwt}
    let postData = this.state.syncItem;
    apiModule('send_notification', request, postData).then(function(data) {
      
    });
    //send_notification
  }

  render() {
    const { selectedOption, user } = this.state;

    return (
      <div className="home">
        <div  className="box">
            <div className="box-wrap" onClick={() => myConfig.sidebarContext.toggleDrawer()}>
              <img className="menu-bar-breed" src={require('../assets/icons/menu.svg')}/>
              <span className="menu-label-breed">Send Notification</span>
            </div>
        </div>
       
        <div className="home-container">
                  
        </div>
        <div className="home-container">
            <div className="row">
              <span>Select recepients</span>
            </div>
            <Select
              id="receipients"
              name="receipients"
              isMulti = {true}
              isSearchable = {true}
              value={selectedOption}
              onChange={this.handleChange}
              options={user}
            />
            <div className="row">
              <span>Title</span>
            </div>
            <input type="text" className="inputText" name="title" id="title" onChange={event => {this.handleChanges(event); }} placeholder="Notification Title"/> <br/>
            <div className="row">
              <span>Message</span>
            </div>

            <textarea name="message" id="message" onChange={event => {this.handleChanges(event); }} className="fullText" id="message" placeholder="Notification Message"/>
            <br/>
            <div className="row">
              
              <label className="regular-label btn btn-info"><u onClick={() => this.sendNotification()}>Send</u></label>
            </div>
        </div>
      </div>
    ); 
  }
}

export default withRouter(Notifications);