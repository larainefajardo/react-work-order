import React, { Component } from 'react';
import '../App.css';
import { writeData, clearAllData, prettifyDate, formatDateByNumber } from '../commons/utility';
import { Modal, Loader } from '../commons/utilityViews.js';
import Sidebar from '../components/sidebar';
import '../home.css';
import DatePicker from 'react-date-picker';
import { apiModule } from '../commons/apiCall.js';
import {Doughnut} from 'react-chartjs-2';
import { myConfig } from '../commons/config';
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import { withRouter } from 'react-router-dom';
import 'react-vertical-timeline-component/style.min.css';
class Inbox extends Component {

  constructor(props) {
    super(props);
    this.state = {
      toggle : undefined,
      messageList : []
    };
  }
  
async componentDidMount(){
  let jwt = this.props.getSession().jwt;
  let user_id = this.props.getSession().user.id;
  this.setState({jwt : jwt});
  await this.fetchMessages(this.props.getSession().jwt, user_id);
}

fetchMessages = async (jwt, user_id) => {
  let headerParams = { x_auth : jwt, user_id : user_id };
  let result = await apiModule('user_notification_inbox', headerParams, null);
  this.setState({messageList : result});
}

displayUserList = () => {
  return this.state.messageList.map((item, i) => {
    let date = formatDateByNumber(item.sent_date);
    return (
        <div key={i} className="col-md-8 inbox-container">
            <div className="row">
              <span className="col inbox-date">{date}</span>
              <span className="inbox-status">NEW</span>
            </div>
            <div className="inbox-message">
              <label>{item.title}</label>
              <label className="message-details">{item.message}</label>
              <span className="learn-more">Learn more</span>
            </div>
          </div>
      )
  });
}

  render() {
    return (
      <div className="home">
        <div  className="box">
          <div className="box-wrap" onClick={() => myConfig.sidebarContext.toggleDrawer()}>
            <img className="menu-bar-breed" src={require('../assets/icons/menu.svg')}/>
            <span className="menu-label-breed">Inbox</span>
          </div>
        </div>
        <div className="align-search">
          <form  className="col-md-4">
            <input type="text" id="search-bar" name="search" value={this.state.search} placeholder="Search message"/>
            <i className="fa fa-search search-icons" ></i>
          </form>
        </div>
        <div className="row flex-wrap">
          {this.displayUserList()}
          
        </div>
      </div>
        
    ); 
  }
}

export default withRouter(Inbox);