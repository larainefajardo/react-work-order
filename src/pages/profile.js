import React, { Component } from 'react';
import '../App.css';
import { writeData, clearAllData, prettifyDate, formatDateByNumber } from '../commons/utility';
import { Modal, Loader } from '../commons/utilityViews.js';
import Sidebar from '../components/sidebar';
import '../home.css';
import DatePicker from 'react-date-picker';
import {Doughnut} from 'react-chartjs-2';
import { myConfig } from '../commons/config';
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import { withRouter } from 'react-router-dom';
import 'react-vertical-timeline-component/style.min.css';
import { styles } from '../styles/styles.js';
import { apiModule } from '../commons/apiCall.js';
class Profile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      toggle : undefined,
      userDetails: {},
      isLoading : true,
      inputDisabled : true,
      profileChanges : 'EDIT PROFILE'
    };
    this.handleProfileChanges = this.handleProfileChanges.bind(this);
    this.handleProfileButton = this.handleProfileButton.bind(this);
  }
  
async componentDidMount(){
  let jwt = this.props.getSession().jwt;
  this.setState({jwt : jwt});
  await this.fetchUserProfile(jwt);
}

async fetchUserProfile(jwt){
  let headerParams = { x_auth : jwt };
  let user = await apiModule('user_profile', headerParams, null );
  this.setState({userDetails : user, isLoading : false})
}

handleProfileChanges(e){
  let userDetails = this.state.userDetails;
  userDetails[e.target.name] = e.target.value;
  this.setState({userDetails});
}

async handleProfileButton(e){
  this.setState({inputDisabled : !this.state.inputDisabled, profileChanges : this.state.inputDisabled ? 'SAVE CHANGES' : 'EDIT PROFILE' });
  if(this.state.profileChanges == "SAVE CHANGES"){
    let headerParams = { user_id : this.state.userDetails.user_id , x_auth : this.state.jwt };
    let user = this.state.userDetails;
    let postData = { first_name : user.first_name, last_name : user.last_name, email : user.email, contact_number : user.contact_number, country : user.country, city : user.city };
    let result = await apiModule("admin_update_user", headerParams, postData);
    window.confirm(result.message)
  }
}

  render() {
    let roleName = this.state.userDetails.role != undefined ? this.state.userDetails.role.name : null;
    return (
      <div className="home" style={styles.templateSkin}>
            <div  className="box">
              <div className="box-wrap" onClick={() => this.props.history.goBack()}>
                <img className="menu-bar-breed" src={require('../assets/icons/back.svg')}/>
                <span className="menu-label-breed">Profile</span>
              </div>
            </div>
            <div className="profile col-md-12 profile-wrapper">
            <form className="form-user-profile">
            <div className="col-md-4 row profile-border">
            <div onClick={(event) => this.handleProfileButton(event)} className="edit-profile">
              <label className="edit-profile-btn">{this.state.profileChanges}</label>
            </div>
              <div className="col row">
                <div className="col-xs-3">
                    <img className="img-card"  src={require('../assets/icons/person-silhouette.png')} /> 
                </div>
                <div className="sidebar-profile col-xs-8">
                    <label className="profile-details">{this.state.userDetails.full_name}</label>
                    <label className="profile-details">{this.state.userDetails.email}</label>
                </div>
              </div>
                <div className="col-12 mt10"><hr /></div>
                <div className="col-md-12 row">
                  <div className="col">
                    <label className="profile-details">First Name:</label>
                  </div>
                  <div className="col">
                    <input type="text" name="first_name" className="profile-details" onChange={(event) => this.handleProfileChanges(event)} disabled={this.state.inputDisabled} value={this.state.userDetails.first_name} />
                  </div>
                </div> 
                <div className="col-md-12"><hr /></div>
                <div className="col-md-12 row">
                  <div className="col">
                    <label className="profile-details">Last Name:</label>
                  </div>
                  <div className="col">
                    <input type="text" className="profile-details" name="last_name" onChange={(event) => this.handleProfileChanges(event)} disabled={this.state.inputDisabled} value={this.state.userDetails.last_name} />
                  </div>
                </div> 
                <div className="col-md-12"><hr /></div>
                <div className="col-md-12 row">
                  <div className="col">
                    <label className="profile-details">Title:</label>
                  </div>
                  <div className="col">
                    <input type="text" className="profile-details" disabled={true} value={roleName} />
                  </div>
                </div>  
                <div className="col-md-12"><hr /></div>
                <div className="col-md-12 row">
                  <div className="col">
                    <label className="profile-details">Contact Number:</label>
                  </div>
                  <div className="col">
                    <input type="text" className="profile-details" name="contact_number" onChange={(event) => this.handleProfileChanges(event)} disabled={this.state.inputDisabled} value={this.state.userDetails.contact_number} />
                  </div>
                </div> 
                <div className="col-md-12"><hr /></div>
                <div className="col-md-12 row">
                  <div className="col">
                    <label className="profile-details">City:</label>
                  </div>
                  <div className="col">
                    <input type="text" className="profile-details"  name="city" onChange={(event) => this.handleProfileChanges(event)} disabled={this.state.inputDisabled} value={this.state.userDetails.city} />
                  </div>
                </div> 
                <div className="col-md-12"><hr /></div>
                <div className="col-md-12 row">
                  <div className="col">
                    <label className="profile-details">Country:</label>
                  </div>
                  <div className="col">
                    <input type="text" className="profile-details"  name="country" onChange={(event) => this.handleProfileChanges(event)} disabled={this.state.inputDisabled} value={this.state.userDetails.country} />
                  </div>
                </div> 
              </div>
                </form>
            </div>
             <Loader show={this.state.isLoading} />
          </div>
        
    ); 
  }
}

export default withRouter(Profile);