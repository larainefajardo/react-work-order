import React, { Component } from 'react';
import './App.css';
import './Sidebar.css';
import './css/Responsive.css';
import { writeData, readAllData } from './commons/utility.js';
import Login from './components/login.js';
import Home from './pages/home';
import Routes from './commons/routes.js';
import { myConfig } from './commons/config.js';
import { BrowserRouter as Router } from 'react-router-dom';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 'SelectFarm',
      session: {
        jwt: '',
        full_name: '',
        user: null,
        
      },
      clientId        : '',
      Id              : 2,
      clientName      : '',
      logo            : '',
      toggleDrawer     : undefined
    };
  }

  async componentDidMount() {
    const parsedData = await readAllData('users');
    if(parsedData != ""){
      let item = {
        jwt: parsedData[0].jwt,
        full_name: parsedData[0].full_name,
        user: parsedData[0].user
      }
      
      this.setState({ session: item })
      
    }
  }

  facebookLogin = () => {
    let that =  this;
    let login_status = 'connected';
    window.fbAsyncInit = function() {
      window.FB.init({
        appId      : myConfig.FACEBOOK_APP_ID,
        cookie     : true,
        xfbml      : true,
        version    : myConfig.FACEBOOK_APP_VERSION
      });

      // Broadcast an event when FB object is ready
      var fbInitEvent = new Event('FBObjectReady');
      document.dispatchEvent(fbInitEvent);
        
    };
      (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }


  toggleSidebar(){
    let drawer = !this.state.toggleDrawer;
    console.log("drawer")
    console.log(drawer)
    this.setState({toggleDrawer : drawer});
  }

  getSession() {
    return this.state.session;
  }
  
  setUserSession(user) {
    this.setState({session: user});
  }
  changePageHandler = (pageId) => {
    this.setState({page: pageId});
  }
  
  checkSession = () => {
    return this.state.session;
  }
  
  // <Timeline/>
  render() {
    console.log("App testing");
    return (
      <Routes
        dbSession={() => this.checkSession()}
        pageId = {this.state.page}
        clientId = {this.state.clientId}
        Id = {this.state.Id}
        clientName = {this.state.clientName}
        logo = {this.state.logo}
        changePageHandler={(pageId) => this.changePageHandler(pageId)}
        changeClientId={(clientId,name , logo,Id) => this.changeClientId(clientId,name , logo,Id)}
        setUserSession={(user) => this.setUserSession(user)}
        getSession={() => this.getSession()}
        toggleSidebar={() => this.toggleSidebar()}
        toggleDrawer={this.state.toggleDrawer}
      >
      </Routes>
    );
  }
}

export default App;
